import scipy.io.wavfile as spiowav
import numpy as np
import sys

class SongHandler:
    
    """Handler dla wczytanego utworu"""

    songPath = None
    rate = None
    time = None
    channels = None

    _errFileNotFoundHandler = None

    def __init__(self, path):
        self.songPath             = path

    def load(self):
        [self.rate, self.channels] = self._loadSongAndHandleErrors()
        self.time = np.linspace(0, self.getNumberOfSamples()/self.rate, self.getNumberOfSamples());

    def setErrFileNotFoundHandler(self, handler):
        self._errFileNotFoundHandler = handler

    def _loadSongAndHandleErrors(self):
        try:
            [rate, sample] = spiowav.read(self.songPath)
        except FileNotFoundError:
            print('SongSample._loadSongAndHandleErrors(): Wrong path to sample!\n Try again.')
            raise FileNotFoundError

        return (rate, sample)


    def getNumberOfChannels(self):
        try:
            self.channels.shape[1]
        except IndexError:
            return 1

        return self.channels.shape[1]


    def getNumberOfSamples(self):
        try:
            self.channels.shape[1]
        except IndexError:
            return len(self.channels)

        return self.channels.shape[0]


    def getRate(self):
        return self.rate

    def getFrame(self, _from, _to, channel):
        if (self.getNumberOfChannels() > 1):
            pass  # TODO
        else:
            return [ self.time[_from:_to],  self.channels[_from:_to]]

    def getMaxTime(self):
        ## TODO:
        return self.time[-1]


    def getAllChannelsWithTimestamp(self):
        result = {'time': self.time}
        if self.getNumberOfChannels() == 1:
            result['Channel1'] = self.channels[:]
            return result
        else:
            for i in range(self.getNumberOfChannels()):
                key = 'Channel' + str(i+1)
                result[key] = self.channels[:,i]
            return result
