from aubio import source
from aubio import onset as fonset
from scipy.signal import medfilt
import matplotlib.pyplot as plt
import var.variables as var
import numpy as np

class Onset:

    """Blok wykrywania początków dźwięków"""

    def __init__(self, filename, rate):

        self.onset = None
        self.max_time = None
        self.rate = rate

        self._win_s = 512
        self._hop_s = self._win_s // 2
        self._threshold = 0.75
        self._spacing_ms = 150

        self._load_song(filename)

    def _load_song(self, filename):

        """Główny algorytm"""

        src = source(filename, self.rate, self._hop_s)
        onset_o = fonset("specdiff", self._win_s, self._hop_s, self.rate)
        onset_o.set_threshold(self._threshold)
        onset_o.set_minioi_ms(self._spacing_ms)

        onsets = []

        total_frames = 0
        while True:
            samples, read = src()
            if onset_o(samples):
                onsets.append(onset_o.get_last_s())
            total_frames += read
            if read < self._hop_s:
                break

        self.max_time = (total_frames / float(self.rate))
        self.onset = onsets

    def get_closest(self, time):

        """Zwraca najbliższy wykryty początek dla podanej chwili czasu"""

        min = 100
        minarg = 100
        for i in range(np.size(self.onset)):
            val = abs(time - self.onset[i])
            if (val < min):
                min = val
                minarg = self.onset[i]

        return minarg

    def disp(self):

        """Wyświetla wykryte początki dźwięków"""

        plt.figure()
        plt.axis(xmin = 0, xmax = self.max_time)
        val = np.ones(np.size(self.onset))
        plt.stem(self.onset, val)
        plt.grid(True)
        plt.title("Wykryte początki dźwięków")
        plt.xlabel("Czas [s]")
        plt.ylabel("Wykryto [bool]")

        plt.show()

        plt.figure()
        plt.axis(xmin = 0, xmax = self.max_time)
        channels = var.songHandler.getAllChannelsWithTimestamp()
        y = channels["Channel1"]
        x = channels["time"]
        plt.plot(x, y, 'm')
        plt.grid(True)
        plt.title("Oryginalny sygnał")
        plt.xlabel("Czas [s]")
        plt.ylabel("Amplituda")

        plt.show()

class COnset(Onset):

    """Klasa pomocnicza"""

    def __init__(self, onset):

        self.onset = onset
        self.max_time = max(onset)
        self.rate = None

        self._win_s = None
        self._hop_s = None
        self._threshold = None
        self._spacing_ms = None
