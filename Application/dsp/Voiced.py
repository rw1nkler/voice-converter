import matplotlib.pyplot as plt
import var.variables as var
import numpy as np
from math import floor
from dsp.alg.acorr import abs_frame
from scipy.signal import medfilt

class Voiced:

    """
    Blok wykrywania aktywności mówcy

    Klasyfikuje poszczególne momenty czasu jako bezgłośne lub takie,
    w których występuje śpiew.
    """

    def __init__(self, rate):

        self.voiced = None
        self.time = None
        self.rate = rate

        self._win_s = 1024
        self._hop_s = self._win_s // 2

        self._max_ener_threshold = 0.06

        self._glob_ener_threshold = 30000

        self._avg_ener_threshold_global = 5000
        self._avg_ener_threshold_relative = 0.05

        self.step_up = None

        self._load_song()

    def _load_song(self):
        """Algorytm określania aktywności mówcy"""

        assert var.songHandler != None, "pitchHandler is None"

        channels = var.songHandler.getAllChannelsWithTimestamp()
        time = channels["time"]
        values = channels["Channel1"]
        samples_num = var.songHandler.getNumberOfSamples()

        self._frames_num = 1 + floor((samples_num - self._win_s) / self._hop_s)

        self._eng = []
        self._frame_time = []

        for i in range(self._frames_num):
            vfrom = int(i * self._hop_s)
            vto   = int(i * self._hop_s + self._win_s)
            self._frame_time += [(vfrom + self._win_s / 2) / self.rate];
            frame = values[vfrom : vto]
            self._eng += [abs_frame(frame)]

        self.max_threshold()

    def max_threshold(self):
        """Klasyfikacja ramek na podstawie ramki o największej wartości średniej amplitud"""

        max_ener = max(self._eng)

        voiced = []
        for i in range(self._frames_num):
            if self._eng[i] / max_ener <= self._max_ener_threshold:
                voiced += [0]
            else:
                voiced += [1]

        self.time = self._frame_time
        self.voiced = voiced

    def filter(self):
        """Proces filtracji i redukcji danych"""

        self.reduce_noises()
        # self.disp()
        self.reduce()

    def reduce_noises(self):
        """Usunięcie zakłóceń z sygnału"""
        time_res = self.time[1] - self.time[0]

        prev_diff = self.voiced[0]
        prev_ind = 0
        prev_good = self.voiced[0]
        count_tmp = 0
        for i in range(np.size(self.voiced)):
            if prev_diff == self.voiced[i]:
                count_tmp += 1
            else:
                if (count_tmp * time_res) >= 0.100:
                    for j in range(prev_ind, i):
                        self.voiced[j] = prev_diff
                        prev_good = prev_diff
                else:
                    for j in range(prev_ind, i):
                        self.voiced[j] = prev_good

                count_tmp = 0
                prev_diff = self.voiced[i]
                prev_ind = i

    def reduce(self):
        """Redukcja ilości danych"""

        voiced2 = []
        vtime2 = []
        prev = -1
        for i in range(np.size(self.voiced)):
            if prev != self.voiced[i]:
                voiced2 += [self.voiced[i]]
                vtime2 += [self.time[i]]
            prev = self.voiced[i]

        self.voiced = voiced2
        self.time = vtime2

    def time_to_voiced(self, time):
        """Zwraca czas do najbliższego momentu zaklasyfikowanego jako obecność śpiewu"""

        prev = 0
        prev_time = 100
        for i in range(np.size(self.voiced)):
            if self.time[i] >= time:
                if self.voiced[i] == 0 and prev == 1:
                    return 0
                elif self.voiced[i] == 1 and prev == 0:
                    return abs(time - self.time[i])
            prev = self.voiced[i]
            prev_time = self.time[i]

        return 100

    def time_to_step_up(self, time):
        """Zwraca czas do najbliższego momentu zmiany aktywności z bezgłośnej
         na obecność śpiewu"""

        if(self.step_up == None):
            self.step_up = []
            prev = 0
            for i in range(np.size(self.voiced)):
                if prev == 0 and self.voiced[i] == 1:
                    self.step_up += [self.time[i]]
                prev = self.voiced[i]

        prev_time = 0
        for i in range(np.size(self.step_up)):
            if self.step_up[i] >= time:
                return abs(time - self.step_up[i])
            prev_time = self.step_up[i]

        return 100


    def disp(self):
        """Wyświetla ocenę aktywności. Używać przed redukcją danych"""

        channels = var.songHandler.getAllChannelsWithTimestamp()
        time = channels["time"]
        values = channels["Channel1"]

        fig = plt.figure()
        axes1 = fig.add_subplot(211)
        max_time = var.songHandler.getMaxTime()
        axes1.axis(xmin = 0, xmax = max_time)
        axes1.plot(self.time, self.voiced)
        axes1.set_title("Dane z bloku wykrywania aktywności mówcy")
        axes1.set_xlabel("Czas [s]")
        axes1.set_ylabel("Aktywność [bool]")

        axes2 = fig.add_subplot(212)
        axes2.axis(xmin = 0, xmax = max_time)
        axes2.plot(time, values, 'm')
        axes2.set_title("Sygnał audio")
        axes2.set_xlabel("Czas [s]")
        axes2.set_ylabel("Amplituda")

        plt.show()

    def pdisp(self):
        """Wyświetla ocenę aktywności. Używać przed redukcją danych"""

        plt.figure()
        max_time = var.songHandler.getMaxTime()
        plt.axis(xmin = 0, xmax = max_time)
        plt.grid(True)
        plt.plot(self.time, self.voiced)
        plt.title("Wprowadzone poprawki odczytów aktywności")
        plt.xlabel("Czas [s]")
        plt.ylabel("Aktywność [bool]")

        plt.show()

    def sdisp(self):
        """Wyświetla ocenę aktywności. Używać po redukcji danych"""

        plt.figure()
        max_time = var.songHandler.getMaxTime()
        plt.axis(xmin = 0, xmax = max_time)
        plt.grid(True)
        plt.stem(self.time, self.voiced)
        plt.title("Zredukowane dane detekcji aktywności")
        plt.xlabel("Czas [s]")
        plt.ylabel("Aktywność [bool]")
        plt.show()
