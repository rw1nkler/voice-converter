import numpy as np
import matplotlib.pyplot as plt
from math import ceil, floor

import var.variables as var

class Yin:

    """Moja implementacja algorytmu Yin"""

    def __init__(self, values, rate, frameSize):
        gap = frameSize/2

        samples = len(values)
        framesNumber = 1 + floor((samples - frameSize)/gap)
        times = []
        freq = []
        var.pitchTable = {}
        for i in range(framesNumber):
            vfrom = int(i*gap)
            vto   = int(i*gap + frameSize)
            time = (vfrom + frameSize/2)/rate;
            val = values[vfrom : vto]
            E = energy(val)

            if E <= 1000000:
                Y = 0
            else:
                Y = self.yin(val, rate, len(val))

            times.append(time)
            freq.append(Y)

            print(str(i + 1) + ": time = " + str(round(time, 5)) + ", freq = " + str(round(Y,2)) + ", eng = " + str(int(E)))

        var.pitchTable["time"] = times
        var.pitchTable["freq"] = freq

    def squaredDifferenceFunction(self, x, Tau):
        SDF = np.zeros(np.size(Tau))

        # Krok 1: obliczenie SDF
        N = np.size(x)
        k = 0
        for tau in Tau:
            for n in range(N):
            # y to zmienna pomocnicza x[n + tau],
            # uwzgledniająca ze poza rozmiarem tablicy jest 0
                if (n + tau >= len(x)):  # x[n + tau]
                    y = 0
                else:
                    y = x[n + tau]

                SDF[k] += (x[n] - y)**2

            SDF[k] = SDF[k]/N
            k=k+1

        return SDF

    def cumulativeMeanNormalizedDifferenceFunction(self, SDF, tau):
        # tau musi być uporzadkowane i co 1, zaczynac sie od 0

        CMNDF = np.zeros(np.size(SDF))
        for i in tau:
            if i == 0:
                CMNDF[i] = 1
            else:
                SUM = 0
                for j in range(1, i+1):
                    SUM += SDF[j]

                SUM = SUM/i
                if (SUM == 0):
                    CMNDF[i] = 1
                else:
                    CMNDF[i] = SDF[i]/SUM

        return CMNDF

    def threshold(self, CMNDF, lvl):
        length = np.size(CMNDF)
        for i in range(length):
            if CMNDF[i] <= lvl:
                return int(i)
        return (np.ndarray.tolist(CMNDF)).index(min(CMNDF))

    def parabolicInterpolation(self, CMNDF, ind):
        x1 = ind - 1
        x2 = ind
        x3 = ind + 1

        y1 = CMNDF[x1]
        y2 = CMNDF[x2]
        y3 = CMNDF[x3]

        X = np.matrix([[x1**2, x1, 1],
                      [x2**2, x2, 1],
                      [x3**2, x3, 1]])

        Y = np.matrix([[y1],
                      [y2],
                      [y3]])

        A = np.linalg.solve(X,Y)

        if A[0] == 0:
          x = x2
        else:
          x = (-A[1]/(2*A[0]))
          x = np.squeeze(np.asarray(x))


        return x

    def convertToFreq(self, x, rate):
        if x == 0:
            return 0
        else:
            val = rate/x
            if val >= rate:
                return 0
            else:
                return val

    def yin(self, x, rate, size):
        tau = range(round(size/4))

        # Krok 1: obliczenie SDF
        SDF = self.squaredDifferenceFunction(x, tau)

        # Krok 2: obliczenie CMSDF
        CMNDF = self.cumulativeMeanNormalizedDifferenceFunction(SDF, tau)

        # Krok 3: threshold
        index = self.threshold(CMNDF, 0.1)

        # Krok 4: Interpolacja paraboliczna
        x = self.parabolicInterpolation(CMNDF, index)

        c = self.convertToFreq(x, rate)
        return c
