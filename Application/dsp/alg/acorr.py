import numpy as np

def acorr(y):

    """Autokorelacja"""

    N = np.size(y)
    corr = np.zeros(2*N + 1)
    corr = np.ndarray.astype(corr, np.float32)
    y = np.ndarray.astype(y, np.float32)
    print(corr)
    for i in range(2*N + 1):
        tau = -N + i
        for n in range(N):
            if (n + tau < 0) or (n + tau >= N):
                tmp = 0
            else:
                tmp = y[n+tau]

            corr[i] += y[n]*tmp
        corr[i] = corr[i]/(N)

    return corr;

def energy(y):

    """Obliczenie energii sygnału"""

    N = np.size(y)
    fpow = 0
    for n in range(N):
        fpow += y[n]**2
    fpow = fpow/N

    return fpow

def abs_frame(y):

    """Obliczenie średniej wartości bezwzględnej amplitud w ramce"""
    
    N = np.size(y)
    abs_val = 0
    for n in range(N):
        abs_val += abs(y[n])
    abs_val = abs_val / N

    return abs_val
