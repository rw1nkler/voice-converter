from aubio import source
from aubio import tempo as ftempo
import numpy as np

class Tempo:

    """Wykrywanie tempa utworu"""

    def __init__(self, filename, rate):

        self.tempo = None
        self.rate = rate

        self._win_s = 512
        self._hop_s = self._win_s // 2

        self._load_song(filename)

    def _load_song(self, filename):

        """Algorytm wykrywania tempa utworu"""

        src = source(filename, self.rate, self._hop_s)
        tempo_o = ftempo("specdiff", self._win_s, self._hop_s, self.rate)

        beats = []
        total_frames = 0

        while True:
            samples, read = src()
            is_beat = tempo_o(samples)
            if is_beat:
                this_beat = tempo_o.get_last_s()
                beats.append(this_beat)
            total_frames += read
            if read < self._hop_s:
                break

        if len(beats) > 1 and len(beats) < 4:
            print("To few beats")

        bpms = 60.0/np.diff(beats)
        self.tempo = np.median(bpms)

    def get_quarter_note_length_s(self):
        """Zwraca estymowany czas trwania ćwierćnuty - BPM"""
        return (60.0 / self.tempo)
