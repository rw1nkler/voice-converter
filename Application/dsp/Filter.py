from scipy.signal import firwin
from scipy.signal import lfilter
import matplotlib.pyplot as plt

class Filter:

    """Filtracja środkowoprzepustowa"""

    def __init__(self, samples, rate):
        self._smp = samples
        self._filtered = None
        self._rate = rate

        # Utworzenie filtru

        order = 300

        freq_top = 1300
        freq_down = 65
        f1 = (freq_down / (rate / 2))
        f2 = (freq_top / (rate / 2))
        self.fir_filter_bandpass = firwin(order, [f1, f2], pass_zero=False)

    def fir(self):

        """Filtracja filtrem FIR"""

        result  = lfilter(self.fir_filter_bandpass, 1, self._smp)
        self._filtered = result
        return result

    def plot(self):

        """Wyświetlenie sygnału po filtracji"""

        plt.figure()
        plt.plot(self._smp)
        plt.hold(True)
        plt.plot(self._filtered)
        plt.show()
