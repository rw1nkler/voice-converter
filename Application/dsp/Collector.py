import var.variables as var
import matplotlib.pyplot as plt
import numpy as np
from music21.stream import Stream
from music21.note import Note
from music21.note import Rest
from music21.interval import Interval
from copy import deepcopy
from scipy.signal import medfilt
import pandas as pd

from dsp.Pitch import Pitch
from dsp.Onset import Onset
from dsp.Onset import COnset
from dsp.Voiced import Voiced
from dsp.Tempo import Tempo

from collections import deque
from statistics import median as medi
from music21.stream.makeNotation import makeMeasures
from music21.stream.makeNotation import makeRests
from music21.meter import TimeSignature
from music21.metadata import Metadata
from music21.tempo import MetronomeMark
from music21.clef import AltoClef, BassClef, TrebleClef

class Collector:

    """Blok decyzyjny"""

    meter_numerator = 4
    meter_denominator = 4

    pauses = 0

    clef = "BassClef"
    clef8 = 0

    tempo = 2

    title = ""
    author = ""

    conversion_mode = "Median"

    def __init__(self, pitch, onset, voiced, tempo):

        assert isinstance(pitch, Pitch),   "Uninitialized pitch"
        assert isinstance(onset, Onset),   "Uninitialized onset"
        assert isinstance(voiced, Voiced), "Uninitialized voiced"
        assert isinstance(tempo, Tempo),   "Uninitialized tempo"

        self.pitch_o = pitch
        self.onset_o = onset
        self.voiced_o = voiced
        self.tempo_o = tempo

        self._detected_onsets = []
        self._possible_onsets = self.onset_o.onset[:]

        self._detected_notes_pitch = []
        self._detected_notes_onsets = []
        self._detected_notes_duration = []

        self.music_sheet = Stream()

    def create_detected_notes_list_pitch(self):

        """Tworzy listę wykrytych wysokości dźwięków"""

        self._detected_notes_pitch = self.pitch_o.pitch
        self._detected_notes_onsets = self.pitch_o.time

    def detect_pitch_onsets(self):

        """Wykrywa te początki dźwięków z bloku wykrywania początków,
        które odpowiadają zmianie wysokości dźwięku z bloku wykrywania wysokości"""

        detected = []
        closest = None
        for i in self.onset_o.onset:
            time_to_pitch = self.pitch_o.time_to_new_pitch(i)
            if time_to_pitch <= 0.150:
                if (not detected) or (self.pitch_o.closest_pitch(detected[-1]) != self.pitch_o.closest_pitch(i)):
                    detected += [i]
                else:
                    if time_to_pitch < self.pitch_o.time_to_new_pitch(detected[-1]):
                        detected[-1] = i

        for i in detected:
            self._possible_onsets.remove(i)
            self._detected_onsets += [i]

        for i in range(np.size(self.pitch_o.time)):
            for j in range(np.size(self._detected_onsets)):
                if abs(self.pitch_o.time[i] - self._detected_onsets[j] <= 0.150):
                    if (not self.pitch_o.time[i] in self._detected_onsets):
                        self._detected_onsets[j] = self.pitch_o.time[i]

        for i in self.pitch_o.time:
            if (not i in self._detected_onsets):
                self._detected_onsets += [i]

        val = self._detected_onsets[0]
        if val <= 0.150:
            self._detected_onsets.remove(val)

        self._detected_onsets.sort()
        self._possible_onsets.sort()

    def remove_unvoiced_onsets(self):

        """Usuwa początki dźwięków wykryte w chwilach bezgłośnych"""

        detected = []
        for i in self._possible_onsets:
            time = self.voiced_o.time_to_voiced(i)
            if time > 0.150:
                detected += [i]

        for i in detected:
            self._possible_onsets.remove(i)

    def remove_to_close_onsets(self):

        """Usuwa początki dźwięków zbyt blisko od siebie"""

        half = self.tempo_o.get_quarter_note_length_s()

        detected = []
        for i in self._possible_onsets:
            for j in self._detected_onsets:
                if (abs(i - j) <= 0.8*(half/2)):
                    if (not i in self._detected_onsets):
                        detected += [i]

        for i in detected:
            if i in self._possible_onsets:
                self._possible_onsets.remove(i)

        detected.clear()

        for i in self._possible_onsets:
            for j in self._possible_onsets:
                if (abs(i - j) <= 0.8*(half/2)) and i != j:
                    if (not i in detected):
                        detected += [i]

        for i in detected:
            self._possible_onsets.remove(i)

    def add_rhytmic_onsets(self):

        """Dodaje do listy wykrytych początków, te przypadające w ważnych momentach
        podf względem rytmiki"""

        all_onsets = COnset(self._detected_onsets[:] + self._possible_onsets[:])

        half = self.tempo_o.get_quarter_note_length_s() / 2
        half_org = half

        half_deque = deque(maxlen = 3)
        half_deque.append(half)

        onsets = all_onsets.onset
        detected = [onsets[0]]
        for i in range(np.size(onsets) - 1):
            for j in range(1, 5):
                next_value = onsets[i] + j * half
                minarg = all_onsets.get_closest(next_value)

                if (abs(minarg  - (onsets[i] + j * half)) <= half / 2):
                    if not minarg in detected:
                        detected += [minarg]

                        new_half = half + abs(minarg - next_value)
                        if (abs(1 - (new_half/half_org)) <= 0.5):
                            half_deque.append(new_half);

                            lam = 0
                            wg_lam = 0
                            j = len(half_deque)
                            for i in list(half_deque):
                                lam += i*j
                                wg_lam += j
                                j = j - 1

                            half = lam/wg_lam
                    break

        for i in detected:
            if (not i in self._detected_onsets):
                self._detected_onsets += [i]

    def create_detected_notes_list(self):

        """Tworzy listy wykrytych nut (wysokości i początków) na podstawie listy wykrytych dźwięków"""

        detected_pitch = []
        for i in range(np.size(self._detected_onsets)):
            closest = self.pitch_o.closest_pitch_with_val(self._detected_onsets[i])
            if closest['time'] <= 0.150:
                detected_pitch += [closest['val']]
            else:
                detected_pitch += [self.pitch_o.pitch_at_time(self._detected_onsets[i])]

        onsets = self._detected_onsets
        onsets, detected_pitch = (list(t) for t in zip(*sorted(zip(onsets, detected_pitch))))

        self._detected_notes_pitch = detected_pitch
        self._detected_onsets = onsets

        self._detected_notes_onsets = onsets[:]

    def add_to_stream_simple(self):

        """Dodanie nut do partytury na podstawie bezwzględnej wysokości.
        Można zobaczyć, że takie podejście nie działa"""

        my_note = Note()

        pitches = self._detected_notes_pitch
        notes_amount = np.size(pitches)
        self.music_sheet.repeatAppend(my_note, notes_amount)

        for i in range(notes_amount):
            self.music_sheet[i].pitch.frequency = pitches[i]


    def add_to_stream_relative(self):

        """Algorytm relatywny konwersji"""

        my_note = Note()

        pitches = self._detected_notes_pitch
        notes_amount = np.size(pitches)
        self.music_sheet.repeatAppend(my_note, notes_amount)

        prev = Note()
        prev.pitch.frequency = pitches[0]
        midi_pitch = prev.pitch.midi
        real = Note()
        real.pitch.midi = midi_pitch

        self.music_sheet[0].pitch.frequency = real.pitch.frequency

        for i in range(1, notes_amount):
            next = Note()
            next.pitch.frequency = pitches[i]
            int = Interval(prev, next)
            k = round(int.cents, -2) // 100

            self.music_sheet[i].pitch.frequency = real.pitch.frequency
            d = self.music_sheet[i].pitch.transpose(k)
            self.music_sheet[i].pitch.frequency = d.frequency
            real.pitch.frequency = self.music_sheet[i].pitch.frequency
            prev = next

    def note_from_prev(self, real, prev, note):

        """Funkcja pomocnicza określająca wysokość analizowanego
        dźwięku na podstawie interwału między dźwiękami"""

        int = Interval(prev, note)
        k = round(int.cents, -2) // 100

        tmp = real.pitch.transpose(k)
        tmp_note = Note()
        tmp_note.pitch.frequency = tmp.frequency

        return tmp_note

    def add_to_stream_median(self, notes_ago_number):

        """Tryb medianowy konwersji"""

        assert notes_ago_number % 2 != 0, "Notes ago number must be odd"

        my_note = Note()                                        # init
        pitches = self._detected_notes_pitch
        onsets = self._detected_onsets
        notes_amount = np.size(pitches)                         # notes in sheet
        self.music_sheet.repeatAppend(my_note, notes_amount)

        prev = Note()
        prev.pitch.frequency = pitches[0]
        midi_pitch = prev.pitch.midi
        real = Note()
        real.pitch.midi = midi_pitch

        self.music_sheet[0].pitch.frequency = real.pitch.frequency

        for i in range(1, 3):
            next = Note()
            next.pitch.frequency = pitches[i]
            int = Interval(prev, next)
            k = round(int.cents, -2) // 100

            self.music_sheet[i].pitch.frequency = real.pitch.frequency
            d = self.music_sheet[i].pitch.transpose(k)
            self.music_sheet[i].pitch.frequency = d.frequency
            real.pitch.frequency = self.music_sheet[i].pitch.frequency
            prev = next

        for i in range(3, notes_amount):
            next = Note()
            next.pitch.frequency = pitches[i]

            if i >= notes_ago_number:
                how_many = notes_ago_number
            else:
                if i % 2 == 0:
                    how_many = i - 1
                else:
                    how_many = i

            notes_ago = []
            for j in range(how_many):
                notes_ago += [Note()]
                ind = i - how_many + j
                notes_ago[j].pitch.frequency = pitches[ind]

            note_from_ago = []
            notes_midi = []
            for j in range(how_many):
                ind = i - how_many + j
                note_from_ago += [self.note_from_prev(self.music_sheet[ind], notes_ago[j], next)]
                notes_midi += [note_from_ago[j].pitch.midi]

            med = medi(notes_midi)
            count = notes_midi.count(med)

            if count >= 2:
                new_note_midi = med
            else:
                new_note_midi = one_ago.pitch.midi
            self.music_sheet[i].pitch.midi = new_note_midi

    def set_notes_duration_difference(self):

        """Liczy czasy trwania nut"""

        onsets = self._detected_notes_onsets
        duration = self._detected_notes_duration
        for i in range(np.size(onsets) - 1):
            duration += [onsets[i + 1] - onsets[i]]
        duration += [duration[-1]]

    def estimate_half_length(self):

        """Oblicza lepszy estymator czasu trwania ósemki"""

        half = self.tempo_o.get_quarter_note_length_s()
        values = []
        for i in self._detected_notes_duration:
            if ((abs(half - i)) < 0.6 * half):
                values += [i]

        half = np.average(values)
        return half


    def set_notes_duration_unvoiced(self):

        """Wykrywa pauzy w utworze"""

        half = self.estimate_half_length()
        eight = half / 2

        vtime = self.voiced_o.time
        voiced = self.voiced_o.voiced

        possible_pauses_duration = []
        possible_pauses_onsets = []

        prev_time = vtime[0]
        prev_val = voiced[0]
        for i in range(np.size(voiced) - 1):
            if prev_val == 0 and voiced[i] == 1:
                possible_pauses_duration += [(vtime[i] - prev_time)]
                possible_pauses_onsets += [prev_time]

            prev_time = vtime[i]
            prev_val = voiced[i]

        if possible_pauses_onsets[0] <= 0.150:
            del possible_pauses_onsets[0]
            del possible_pauses_duration[0]

        for i in range(np.size(possible_pauses_duration)):
            if (possible_pauses_duration[i] / eight) >= 0.85:

                prev_voiced_onset = self.get_first_onset_before(possible_pauses_onsets[i])

                pause_length = round((possible_pauses_duration[i] / half) * 2) / 2

                ind = self._detected_notes_onsets.index(prev_voiced_onset)
                new_length = self.music_sheet[ind].duration.quarterLength - pause_length
                if (new_length > 0):
                    rest = Rest()
                    rest.duration.quarterLength = pause_length
                    self.music_sheet[ind].duration.quarterLength = new_length

        makeRests(self.music_sheet, fillGaps=True, inPlace=True)

    def get_first_onset_before(self, time):

        """Zwraca pierwszy początek nuty przed danym momentem czasu"""

        for i in range(np.size(self._detected_notes_onsets)):
            if self._detected_notes_onsets[i] > time:
                return self._detected_notes_onsets[i - 1]

    def display_notes_duration(self):

        """Wyświetla czasy trwania nut"""

        fig = plt.figure()
        plt.hold(True)

        axes1 = fig.add_subplot(211)
        max_time = var.songHandler.getMaxTime()
        axes1.stem(self._detected_notes_onsets, self._detected_notes_pitch, 'r')
        axes1.set_title("Notes")
        axes1.set_xlabel("Time [s]")
        axes1.set_ylabel("All")

        axes2 = fig.add_subplot(212)
        axes2.stem(self._detected_notes_onsets, self._detected_notes_duration, 'g')
        axes2.set_title("Duration")
        axes2.set_xlabel("Time [s]")
        axes2.set_ylabel("Weight")

        plt.show()

    def display_onsets_state(self, stitle = "Onsets"):

        """Wyświetla stan list wykrytych początków i potencjalnych początków"""

        max_time = var.songHandler.getMaxTime()

        plt.figure()
        plt.axis(xmin = 0, xmax = max_time)
        if self._possible_onsets:
            plt.stem(self._possible_onsets, np.ones(np.size(self._possible_onsets)), 'r')
        if self._detected_onsets:
            plt.stem(self._detected_onsets, np.ones(np.size(self._detected_onsets)), 'g')
        plt.grid(True)
        plt.title(stitle)
        plt.xlabel("Czas [s]")
        plt.ylabel("Obecność początku [bool]")

        plt.show()

    def set_notes_duration(self):

        """Określa czas trwania nuty"""

        half = self.estimate_half_length()           # Prediction
        duration = self._detected_notes_duration

        for i in range(np.size(duration)):
            note_length = round((duration[i] / half) * 2) / 2
            self.music_sheet[i].duration.quarterLength = note_length

        music_sheet2 = Stream()
        for i in range(len(self.music_sheet)):
            music_sheet2.append(self.music_sheet[i])

        self.music_sheet = music_sheet2

    def set_proper_meter(self):

        """Ustawia wybrane metrum utworu"""

        meter = TimeSignature()
        meter.numerator = Collector.meter_numerator
        meter.denominator = Collector.meter_denominator

        self.music_sheet.insert(0, meter)

    def analyze_key(self):

        """Analizuje tonację utworu"""

        key = self.music_sheet.analyze('key')
        self.music_sheet.insert(0, key)

    def display_score(self):

        """Wyświetla partyturę, jeżeli włączony jest tryb wyświetlania"""

        self.music_sheet.show()

    def write_score(self, format, file):

        """Zapisuje plik do formatu wyjściowego"""

        self.music_sheet.write(format, file)

    def set_metadata(self):

        """Ustawia w partyturze informacje dotyczące autora i tytułu utworu"""

        self.music_sheet.insert(0, Metadata())

        if Collector.title != "":
            self.music_sheet.metadata.title = Collector.title
        else:
            self.music_sheet.metadata.title = 'Music Converter'

        if Collector.author != "":
            self.music_sheet.metadata.composer = Collector.author
        else:
            self.music_sheet.metadata.composer = 'Music Converter'

    def set_tempo(self):

        """Ustawia w partyturze informację o tempie utworu"""

        tempo = round(self.tempo_o.tempo, -1)
        self.music_sheet.insert(0, MetronomeMark(number=tempo))

    def set_clef(self):

        """Usytawia w partyturze wybrany klucz muzyczny"""

        clef = None
        if Collector.clef == "TrebleClef":
            clef = TrebleClef()
        elif Collector.clef == "BassClef":
            clef = BassClef()
        elif Collector.clef == "AltoClef":
            clef = AltoClef()

        if Collector.clef8 != 0:
            clef.octaveChange = -1
        else:
            clef.octaveChange = 0

        self.music_sheet.insert(0, clef)

    def convert(self, format, file, mode = "Normal"):

        """Algorytm konwersji - głowny algorytm"""

        if (mode == "Normal"):
            print("Mode normal")
            # self.display_onsets_state("Początkowy stan LPP")
            self.detect_pitch_onsets()
            # self.display_onsets_state("LPP i LZP po wykryciu odpowiedników między danymi z bloków")
            self.remove_unvoiced_onsets()
            # self.display_onsets_state("LPP i LZP po usunięciu początków wykrytych w ciszy")
            self.remove_to_close_onsets()
            # self.display_onsets_state("LPP i LZP po usunięciu zbyut bliskich początków")
            self.add_rhytmic_onsets()
            # self.display_onsets_state("LPP i LZP po wykryciu ważnych rytmicznie początków")

            self.create_detected_notes_list()

        else:
            print("Mode pitch")
            self.create_detected_notes_list_pitch()

        if (Collector.conversion_mode == "Median"):
            self.add_to_stream_median(15)
        else:
            self.add_to_stream_relative()

        self.set_notes_duration_difference()
        self.set_notes_duration()

        if (Collector.pauses == 0):
            self.set_notes_duration_unvoiced()

        self.analyze_key()
        self.set_proper_meter()

        if (Collector.tempo != 0):
            self.set_tempo()

        self.set_metadata()
        self.set_clef()

        if format != "disp":
            self.write_score(format, file)
        else:
            self.display_score()

    def pitch_at_time(self, time):

        """Zwraca wysokość nuty w danym momencie czasu i jej czas"""

        for i in range(np.size(self._detected_notes_onsets)):
            if (self._detected_notes_onsets[i] > time):
                ret = dict()
                ret['val'] = self._detected_notes_pitch[i-1]
                ret['time'] = self._detected_notes_onsets[i-1]
                return ret
        ret = dict()
        ret['val'] = self._detected_notes_pitch[-1]
        ret['time'] = self._detected_notes_onsets[-1]
        return ret

    def get_closest_onset(self, time):

        """Zwraca najbliższy początek dźwięku"""
        
        min = 100
        minarg = 100
        for i in range(np.size(self._detected_onsets)):
            val = abs(time - self._detected_onsets[i])
            if (val < min):
                min = val
                minarg = self._detected_onsets[i]
        return minarg

    # def generate_tests2(self):
    #     panie_pa_zen = [1.175, 1.607, 2.101, 2.646, 3.152, 3.679, 4.191, 4.720, \
    #                     5.251, 5.770, 6.264, 7.240, 7.723, 8.293, 9.282, 9.538, \
    #                     9.798, 10.005, 10.305, 10.797, 11.308, 11.559, 11.800,  \
    #                     12.049, 12.322, 12.821, 13.323, 13.851, 14.383, 15.408, \
    #                     15.932, 16.434]
    #
    #     panie_tekst_zen = [1.177, 1.664, 2.130, 2.713, 3.145, 3.680, 4.157, 4.651, \
    #                        5.138, 5.662, 6.029, 7.139, 7.694, 8.205, 9.182, 9.433, \
    #                        9.673, 9.865, 10.159, 10.636, 11.150, 11.425, 11.651, \
    #                        11.829, 12.104, 12.625, 13.129, 13.574, 14.140, 15.145, \
    #                        15.639, 16.187]
    #
    #     oda_pa_zen = [1.347, 1.803, 2.295, 2.755, 3.243, 3.730, 4.240, 4.737, 5.247, \
    #                   5.757, 6.254, 6.755, 7.275, 8.004, 8.272, 9.321, 9.809, 10.296, \
    #                   10.797, 11.294, 11.800, 12.296, 12.820, 13.308, 13.845, 14.333, \
    #                   14.852, 15.358, 16.123, 16.378, 17.367, 17.859, 18.356, 18.852, \
    #                   19.376, 19.873, 20.105, 20.406, 20.875, 21.376, 21.868, 22.105, \
    #                   22.406,  22.893, 23.376, 23.877, 24.369, 24.939, 25.936, 26.447, \
    #                   26.943, 27.431, 27.900, 28.365, 28.884, 29.353, 29.873, 30.383, \
    #                   30.884, 31.399, 32.183, 32.447]
    #
    #     oda_tekst_zen = [2.218, 2.700, 3.104, 3.572, 4.100, 4.591, 5.081, 5.577, 6.104, 6.631, \
    #                      7.122, 7.581, 8.113, 8.881, 9.181, 10.286, 10.727, 11.240, \
    #                      11.758, 12.276, 12.776, 13.258, 13.740, 14.290, 14.735, 15.276, \
    #                      15.708, 16.231, 16.967, 17.303, 18.308, 18.776, 19.344, 19.817, \
    #                      20.330, 20.821, 21.039, 21.312, 21.794, 22.298, 22.867, 23.103, \
    #                      23.376, 23.835, 24.376, 24.853, 25.289, 25.985, 26.962, 27.389, \
    #                      27.975, 28.443, 28.934, 29.384, 29.921, 30.443, 30.971, 31.475, \
    #                      31.984, 32.411, 33.175, 33.457]
    #
    #     panie_notes = ['E4', 'F#4', 'G#4', 'E4', 'E4', 'F#4', 'G#4', 'E4', 'G#4', \
    #                     'A4', 'B4', 'G#4', 'A4', 'B4', 'B4', 'C#5', 'B4', 'A4',   \
    #                     'G#4', 'E4', 'B4', 'C#5', 'B4', 'A4', 'G#4', 'E4', 'F#4', \
    #                     'B3', 'E4', 'F#4', 'B3', 'E4']
    #
    #     oda_notes = ['A4', 'A4', 'B-4', 'C5', 'C5', 'B-4', 'A4', 'G4', 'F4', 'F4', \
    #                  'G4', 'A4', 'A4', 'G4', 'G4', 'A4', 'A4', 'B-4', 'C5', 'C5', \
    #                  'B-4', 'A4', 'G4', 'F4', 'F4', 'G4', 'A4', 'G4', 'F4', 'F4', \
    #                  'G4', 'G4', 'A4', 'F4', 'G4', 'A4', 'B-4', 'A4', 'F4',  'G4',\
    #                  'A4', 'B-4', 'A4', 'G4', 'F4', 'G4', 'C4', 'A4', 'A4', 'B-4', \
    #                  'C5', 'C5', 'B-4', 'A4', 'G4', 'F4', 'F4', 'G4', 'A4', 'G4', \
    #                  'F4', 'F4']
    #
    #     panie_duration = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2, 0.5, 0.5, 0.5, 0.5,\
    #                       1, 1, 0.5, 0.5, 0.5, 0.5, 1, 1, 1, 1, 2, 1, 1, 2]
    #
    #     oda_duration = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1.5, 0.5, 2, 1, 1, 1, 1,  \
    #                     1, 1, 1, 1, 1, 1, 1, 1, 1.5, 0.5, 2, 1, 1, 1, 1, 1, 0.5, 0.5, \
    #                     1, 1, 1, 0.5, 0.5, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1,  \
    #                     1, 1, 1, 1.5, 0.5, 2]
    #
    #
    #     measured_time = oda_tekst_zen
    #     measured_note = oda_notes
    #
    #     measured_note_s = Stream()
    #     for i in measured_note:
    #         measured_note_s.append(Note(i))
    #
    #     measured_duration = oda_duration
    #
    #     data = pd.DataFrame({'t_m': measured_time,\
    #                          'n_m': measured_note,\
    #                          'd_m' : measured_duration,\
    #                          \
    #                          'td_bw' : measured_note,\
    #                          'f_bw':   measured_note,\
    #                          'fd_bw':  measured_note,\
    #                          'i_bw':   measured_note})
    #
    #     print(data.to_latex())
    #
    #
    # def generate_tests(self):
    #     panie_pa_zen = [1.175, 1.607, 2.101, 2.646, 3.152, 3.679, 4.191, 4.720, \
    #                     5.251, 5.770, 6.264, 7.240, 7.723, 8.293, 9.282, 9.538, \
    #                     9.798, 10.005, 10.305, 10.797, 11.308, 11.559, 11.800,  \
    #                     12.049, 12.322, 12.821, 13.323, 13.851, 14.383, 15.408, \
    #                     15.932, 16.434]
    #
    #     panie_tekst_zen = [1.177, 1.664, 2.130, 2.713, 3.145, 3.680, 4.157, 4.651, \
    #                        5.138, 5.662, 6.029, 7.139, 7.694, 8.205, 9.182, 9.433, \
    #                        9.673, 9.865, 10.159, 10.636, 11.150, 11.425, 11.651, \
    #                        11.829, 12.104, 12.625, 13.129, 13.574, 14.140, 15.145, \
    #                        15.639, 16.187]
    #
    #     oda_pa_zen = [1.347, 1.803, 2.295, 2.755, 3.243, 3.730, 4.240, 4.737, 5.247, \
    #                   5.757, 6.254, 6.755, 7.275, 8.004, 8.272, 9.321, 9.809, 10.296, \
    #                   10.797, 11.294, 11.800, 12.296, 12.820, 13.308, 13.845, 14.333, \
    #                   14.852, 15.358, 16.123, 16.378, 17.367, 17.859, 18.356, 18.852, \
    #                   19.376, 19.873, 20.105, 20.406, 20.875, 21.376, 21.868, 22.105, \
    #                   22.406,  22.893, 23.376, 23.877, 24.369, 24.939, 25.936, 26.447, \
    #                   26.943, 27.431, 27.900, 28.365, 28.884, 29.353, 29.873, 30.383, \
    #                   30.884, 31.399, 32.183, 32.447]
    #
    #     oda_tekst_zen = [2.218, 2.700, 3.104, 3.572, 4.100, 4.591, 5.081, 5.577, 6.104, 6.631, \
    #                      7.122, 7.581, 8.113, 8.881, 9.181, 10.286, 10.727, 11.240, \
    #                      11.758, 12.276, 12.776, 13.258, 13.740, 14.290, 14.735, 15.276, \
    #                      15.708, 16.231, 16.967, 17.303, 18.308, 18.776, 19.344, 19.817, \
    #                      20.330, 20.821, 21.039, 21.312, 21.794, 22.298, 22.867, 23.103, \
    #                      23.376, 23.835, 24.376, 24.853, 25.289, 25.985, 26.962, 27.389, \
    #                      27.975, 28.443, 28.934, 29.384, 29.921, 30.443, 30.971, 31.475, \
    #                      31.984, 32.411, 33.175, 33.457]
    #
    #     panie_notes = ['E4', 'F#4', 'G#4', 'E4', 'E4', 'F#4', 'G#4', 'E4', 'G#4', \
    #                     'A4', 'B4', 'G#4', 'A4', 'B4', 'B4', 'C#5', 'B4', 'A4',   \
    #                     'G#4', 'E4', 'B4', 'C#5', 'B4', 'A4', 'G#4', 'E4', 'F#4', \
    #                     'B3', 'E4', 'F#4', 'B3', 'E4']
    #
    #     oda_notes = ['A4', 'A4', 'B-4', 'C5', 'C5', 'B-4', 'A4', 'G4', 'F4', 'F4', \
    #                  'G4', 'A4', 'A4', 'G4', 'G4', 'A4', 'A4', 'B-4', 'C5', 'C5', \
    #                  'B-4', 'A4', 'G4', 'F4', 'F4', 'G4', 'A4', 'G4', 'F4', 'F4', \
    #                  'G4', 'G4', 'A4', 'F4', 'G4', 'A4', 'B-4', 'A4', 'F4',  'G4',\
    #                  'A4', 'B-4', 'A4', 'G4', 'F4', 'G4', 'C4', 'A4', 'A4', 'B-4', \
    #                  'C5', 'C5', 'B-4', 'A4', 'G4', 'F4', 'F4', 'G4', 'A4', 'G4', \
    #                  'F4', 'F4']
    #
    #     # print("size: oda_pa_zen " + str(np.size(oda_pa_zen)))
    #     # print("size: oda_notes " + str(np.size(oda_notes)))
    #
    #     measured_time = oda_tekst_zen
    #     measured_note = oda_notes
    #
    #     measured_note_s = Stream()
    #     for i in measured_note:
    #         measured_note_s.append(Note(i))
    #
    #     measured_interval = []
    #     for i in range(len(measured_note_s) - 1):
    #         int = Interval(measured_note_s[i], measured_note_s[i + 1])
    #         measured_interval += [round(abs(int.cents))]
    #     measured_interval += [0]
    #
    #     print("size: measured_note_s " + str(len(measured_note_s)))
    #
    #     measured_freq = []
    #     for i in measured_note_s:
    #         measured_freq += [round(i.pitch.frequency, 2)]
    #
    #     print("size: measured_freq " + str(np.size(measured_freq)))
    #
    #     closest_pitch_time = []
    #     closest_pitch = []
    #     closest_onset = []
    #     is_voiced = []
    #
    #     for i in measured_time:
    #         ret = self.pitch_o.pitch_at_time2(i + 0.1)
    #         print(ret)
    #         closest_pitch_time += [round(ret['time'] ,3)]
    #         closest_pitch += [round(ret['val'],2)]
    #         closest_onset += [round(self.onset_o.get_closest(i),3)]
    #         is_voiced += [round(self.voiced_o.time_to_voiced(i), 3)]
    #
    #     closest_pitch_s = Stream()
    #     for i in closest_pitch:
    #         tmp = Note()
    #         tmp.pitch.frequency = i
    #         closest_pitch_s.append(tmp)
    #
    #     diff_pitch_cents = []
    #     for i in range(len(closest_pitch_s)):
    #         int = Interval(measured_note_s[i], closest_pitch_s[i])
    #         diff_pitch_cents += [round(abs(int.cents))]
    #
    #     read_interval = []
    #     for i in range(len(closest_pitch_s) - 1):
    #         int = Interval(closest_pitch_s[i], closest_pitch_s[i+1])
    #         read_interval += [round(abs(int.cents))]
    #     read_interval += [0]
    #
    #     diff_interval = []
    #     for i in range(len(read_interval)):
    #         diff_interval += [round(abs(read_interval[i] - measured_interval[i]))]
    #
    #     print("size: closest_pitch_time " + str(np.size(closest_pitch_time)))
    #     print("size: closest_pitch " + str(np.size(closest_pitch)))
    #     print("size: closest_onset " + str(np.size(closest_onset)))
    #     print("size: is_voiced " + str(np.size(is_voiced)))
    #
    #     pitch_time_diff = []
    #     pitch_diff = []
    #     onset_diff = []
    #     for i in range(np.size(measured_time)):
    #         pitch_time_diff += [round(abs(closest_pitch_time[i] - measured_time[i]),3)]
    #         pitch_diff += [round(abs(measured_freq[i] - closest_pitch[i]),2)]
    #         onset_diff += [round(abs(closest_onset[i] - measured_time[i]), 3)]
    #
    #     print("size: time_diff " + str(np.size(pitch_time_diff)))
    #     print("size: pitch_diff " + str(np.size(pitch_diff)))
    #     print("size: onset_diff " + str(np.size(onset_diff)))
    #
    #     lp = range(np.size(measured_time))
    #     for i in lp:
    #         i = i+1
    #     data = pd.DataFrame({'t_m': measured_time,\
    #                         'f_m': measured_freq,\
    #                         'i_m' : measured_interval,\
    #                         \
    #                         'td_bw' : pitch_time_diff,\
    #                         'f_bw': closest_pitch,\
    #                         'fd_bw': diff_pitch_cents,
    #                         'i_bw': read_interval,\
    #                         'id_bw': diff_interval,\
    #                         'p_bw': lp,\
    #                         \
    #                         't_bp [s]': closest_onset,\
    #                         'td_bp [s]': onset_diff,\
    #                         'p_bp': lp,\
    #                         \
    #                         't_ba [s]': is_voiced,\
    #                         'p_ba': lp})
    #     print(data.to_latex())
    #
    #     print("Wykryte wysokośnci dżwięków: " + str(np.size(self.pitch_o.pitch)))
    #     print("Wykryte początki dźwięków: " + str(np.size(self.onset_o.onset)))
    #     print("Dobre: " + str(np.size(measured_time)))
