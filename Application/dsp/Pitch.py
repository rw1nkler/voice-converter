from aubio import source
from aubio import pitch as fpitch
from scipy.signal import medfilt
import matplotlib.pyplot as plt
import var.variables as var
import numpy as np

from music21.note import Note
from music21.interval import Interval

class Pitch:

    """Blok wykrywania wysokości dźwięku"""

    def __init__(self, filename, rate):

        self.time = None
        self.pitch = None
        self.conf = None
        self.rate = rate

        self._win_s = 1400
        self._hop_s = self._win_s // 2
        self._tolerance = 0.1

        self._load_song(filename)

    def _load_song(self, filename):

        """Konwersja algorytmem Yin"""

        src = source(filename, self.rate, self._hop_s)

        pitch_o = fpitch("yin", self._win_s, self._hop_s, self.rate)
        pitch_o.set_unit("Hz")
        pitch_o.set_tolerance(self._tolerance)

        pitches = []
        times = []
        confs = []

        total_frames = 0
        while True:
            samples, read = src()
            pitch = pitch_o(samples)[0]
            pitch = round(pitch)
            confidence = pitch_o.get_confidence()
            if confidence < self._tolerance:
                pitch = 0.0
            times += [total_frames / float(self.rate)]
            pitches += [pitch]
            confs += [confidence]
            total_frames += read
            if read < self._hop_s:
                break

        self.time = times
        self.pitch = pitches
        self.conf = confs

    def compensate_fir_filter(self, order):

        """Przesunięcie próbek, aby nie zawierały opóźnienia filtru"""

        timediff = ((order / 2) / self.rate)
        for i in range(np.size(self.time)):
            self.time[i] = self.time[i] - timediff
            if (self.time[i] < 0):
                self.time[i] = 0

    def exclude_unvoiced(self, voiced):

        """Usunięcie odczytów wysokości z chwil zaklasyfikowanych jako bezgłośne"""

        prev = 0
        for i in range(1, np.size(voiced.voiced)):
            if voiced.voiced[prev] == 0 and voiced.voiced[i] == 0:
                pass
            elif voiced.voiced[prev] == 0 and voiced.voiced[i] == 1:
                _from = self.closest_pitch(voiced.time[prev])
                _to   = self.closest_pitch(voiced.time[i])
                _from = self.time.index(_from)
                _to   = self.time.index(_to)

                for j in range(_from, _to + 1):
                    self.pitch[j] = 0
                prev = i
            else:
                prev = i

        if voiced.voiced[-1] == 0:
            _from = self.closest_pitch(voiced.time[-1])
        else:
            _from = self.closest_pitch(voiced.time[-1])

        _from = self.time.index(_from)
        for j in range(_from, np.size(self.pitch) - 1):
            self.pitch[j] = 0

    def median_filter(self):

        """Filtracja medianowa"""

        pitch2 = medfilt(self.pitch, 11)
        for i in range(len(self.pitch)):
            if int(self.pitch[i]) < 50:
                pitch2[i] = 0.0

        self.pitch = pitch2

    def supress_holes(self):

        """Zapełnienie luk w odczytach"""

        prev = self.pitch[0]
        for i in range(len(self.pitch)):
            if self.pitch[i] == 0:
                self.pitch[i] = prev
            prev = self.pitch[i]

    def reduce_noises(self):

        """Usunięcie zakłóceń (150ms)"""

        time_res = self.time[1] - self.time[0]

        prev_diff = self.pitch[0]
        prev_ind = 0
        prev_good = self.pitch[0]
        count_tmp = 0
        for i in range(np.size(self.pitch)):
            if prev_diff == self.pitch[i]:
                count_tmp += 1
            else:
                if (count_tmp * time_res) >= 0.150:
                    for j in range(prev_ind, i):
                        self.pitch[j] = prev_diff
                        prev_good = prev_diff
                else:
                    for j in range(prev_ind, i):
                        self.pitch[j] = prev_good

                count_tmp = 0
                prev_diff = self.pitch[i]
                prev_ind = i


    def _create_note(self, freq):

        """Funkcja pomocnicza zwracająca obikt Nuty z wpisaną częstotliwością"""

        if (freq < 10):
            freq = 1
        ret = Note()
        ret.pitch.frequency = freq
        return ret

    def octave_error(self):

        """Usuwanie błędów oktaw"""

        half_tone = (2**(1/12)) - 1
        dtime = []
        dindex = []
        pause = False
        for i in range(np.size(self.pitch) - 1):
            norm = (half_tone * self.pitch[i+1])
            if norm != 0:
                dpitch = (self.pitch[i+1] - self.pitch[i]) / norm
                pause = False
                if abs(dpitch) >= 0.75:
                    dtime += [self.time[i+1]]
                    dindex += [i+1]
            else:
                if (pause != True):
                    dtime += [self.time[i+1]]
                    dindex += [i+1]
                    pause = True

        pitches = []
        for i in range(np.size(dindex) - 1):
            pitches += [np.average(self.pitch[dindex[i] : dindex[i+1]])]
        pitches += [np.average(self.pitch[dindex[-1]:])]

        sum = []
        for i in pitches:
            if i > 10:
                sum += [i]

        avg = np.average(sum)
        avg_note = Note()
        avg_note.pitch.frequency = avg

        prev = pitches[0]
        for i in range(1, np.size(pitches) - 1):
            left = pitches[i-1]
            left_note = self._create_note(left)
            mid = pitches[i]
            mid_note = self._create_note(mid)
            right = pitches[i+1]
            right_note = self._create_note(right)

            if (left < 10) and (right < 10) and (mid > 10):  # .|.
                inter = Interval(avg_note, mid_note)
                # print("Przypadek 1: time = " + str(dtime[i]) + " inter = " + str(inter.cents) + ", " + str(abs(inter.cents) >= 1000))
                if (abs(inter.cents) >= 1000):
                    pitches[i] = pitches[i] / 2
                    ind1 = self.time.index(dtime[i])
                    ind2 = self.time.index(dtime[i+1])
                    if (mid > avg):
                        self.pitch[ind1:ind2] = np.average(self.pitch[ind1:ind2]) / 2
                    else:
                        self.pitch[ind1:ind2] = np.average(self.pitch[ind1:ind2]) * 2
            elif (left > 10) and (right <10) and (mid > 10):  # ||.
                inter = Interval(avg_note, mid_note)
                inter2 = Interval(left_note, mid_note)
                # print("Przypadek 2: time = " + str(dtime[i]) + " inter_avg = " + str(inter.cents) + " inter_left = " + str(inter2.cents) + ", " + str(abs(inter.cents) >= 800 and abs(inter2.cents) >= 1150))
                if (abs(inter.cents) >= 800 and abs(inter2.cents) >= 1150):
                    pitches[i] = pitches[i] / 2
                    ind1 = self.time.index(dtime[i])
                    ind2 = self.time.index(dtime[i+1])
                    if (mid > avg):
                        self.pitch[ind1:ind2] = np.average(self.pitch[ind1:ind2]) / 2
                    else:
                        self.pitch[ind1:ind2] = np.average(self.pitch[ind1:ind2]) * 2
            elif (left < 10) and (right > 10) and (mid > 10):  # .||
                inter = Interval(avg_note, mid_note)
                inter2 = Interval(right_note, mid_note)
                # print("Przypadek 3: time = " + str(dtime[i]) + " inter_avg = " + str(inter.cents) + " inter_right = " + str(inter2.cents) + ", " + str(abs(inter.cents) >= 800 and abs(inter2.cents) >= 1150))
                if (abs(inter.cents) >= 800 and abs(inter2.cents) >= 1150):
                    pitches[i] = pitches[i] / 2
                    ind1 = self.time.index(dtime[i])
                    ind2 = self.time.index(dtime[i+1])
                    if (mid > avg):
                        self.pitch[ind1:ind2] = np.average(self.pitch[ind1:ind2]) / 2
                    else:
                        self.pitch[ind1:ind2] = np.average(self.pitch[ind1:ind2]) * 2
            elif (left > 10) and (right > 10) and (mid > 10):   # |||
                inter1 = Interval(mid_note, left_note)
                inter2 = Interval(mid_note, right_note)
                # print("Przypadek 4: time = " + str(dtime[i]) + " inter_left = " + str(inter1.cents) + " inter_right = " + str(inter2.cents) + ", " + str(abs(inter1.cents) + abs(inter2.cents) >= 2000))
                if (abs(inter1.cents) + abs(inter2.cents) >= 2000):
                    pitches[i] = pitches[i] / 2
                    ind1 = self.time.index(dtime[i])
                    ind2 = self.time.index(dtime[i+1])
                    if (mid > avg):
                        self.pitch[ind1:ind2] = np.average(self.pitch[ind1:ind2]) / 2
                    else:
                        self.pitch[ind1:ind2] = np.average(self.pitch[ind1:ind2]) * 2


    def make_rectangular(self):

        """Wstępna klasyfikacja wyników"""

        self.pitch = np.array(self.pitch)

        j = 0                                             # Ukwadratowienie
        half_tone = (2**(1/12)) - 1
        for i in range(len(self.pitch)):
            avg = np.average(self.pitch[j:i])
            if abs(self.pitch[i] - avg) > 0.75 * half_tone * avg:
                self.pitch[j:i] = avg
                j = i
        i = len(self.pitch) - 1
        self.pitch[j:i] = np.average(self.pitch[j:i])

    def reduction(self):

        """Usunięcie błędów w detekcji i redukcja danych"""

        dtime = []
        for i in range(len(self.pitch) - 1):
            norm = (1 - 2**(1/12)) * self.pitch[i+1]
            if norm != 0:
                dpitch = (self.pitch[i+1] - self.pitch[i]) / (norm)
            else:
                dpitch = 0
            if abs(dpitch) >= 0.75:
                dtime += [self.time[i+1]]
        exclude = []
        prev = -1;
        for i in dtime:
            if abs(prev - i) <= 0.15:
                if not (prev in exclude):
                    exclude += [prev]
                    prev = i
            else:
                prev = i

        for i in exclude:
            dtime.remove(i)

        time4 = [0] + dtime                           # Redukcja danych
        pitch4 = []
        prev = 0
        for i in range(1, np.size(time4)):
            ind = self.time.index(time4[i])
            avg = np.average(self.pitch[prev: ind - 1])
            pitch4 += [avg]
            prev = ind

        avg = np.average(self.pitch[ind:])
        pitch4 += [avg]

        if pitch4[0] <= 20:
            del pitch4[0]
            del time4[0]

        self.pitch = pitch4
        self.time = time4

    def filter(self):

        """Przebieg filtracji - główny algorytm"""

        self.median_filter()
        # self.pdisp("Filtracja medianowa")
        self.make_rectangular()
        # self.pdisp("Wstępna klasyfikacja wyników")
        self.reduce_noises()
        # self.pdisp("Korekcja błędów")
        self.octave_error()
        # self.pdisp("Rozwiązanie błędów oktaw")
        self.supress_holes()
        # self.pdisp("Zapełnienie luk w detekcji")
        self.make_rectangular()
        # self.pdisp("Ostateczna klasyfikacja")
        self.median_filter()
        # self.pdisp("Filtracja medianowa")
        self.reduction()
        # self.sdisp("Wykryte wysokości dźwięków")

    def time_to_new_pitch(self, time):

        """Zwraca czas do najbliższego, zaklasyfikowanego dźwięku"""

        min = 100
        minarg = 100
        for i in range(np.size(self.time)):
            diff = abs(self.time[i] - time)
            if (diff < min):
                min = diff
                minarg = i

        return abs(time - self.time[minarg])

    def closest_pitch(self, time):

        """Zwraca najbliższą dla danego czasu, wykrytą wysokość dźwięku"""

        min = 100
        minarg = 100
        for i in range(np.size(self.time)):
            diff = abs(self.time[i] - time)
            if (diff < min):
                min = diff
                minarg = i

        return self.time[minarg]

    def closest_pitch_with_val(self, time):

        """Zwraca najbliższą dla danego czasu, wykrytą wysokość dźwięku i jej czas"""

        min = 100
        minarg = 100
        for i in range(np.size(self.time)):
            diff = abs(self.time[i] - time)
            if (diff < min):
                min = diff
                minarg = i

        ret = dict()
        ret['val'] = self.pitch[minarg]
        ret['time'] = self.time[minarg]

        return ret

    def pitch_at_time(self, time):

        """Zwraca wysokość dźwięku w danym czasie"""

        for i in range(np.size(self.time)):
            if (self.time[i] > time):
                return self.pitch[i - 1]
        return self.pitch[-1]

    def pitch_at_time2(self, time):

        """Zwraca wysokość dźwięku w danym czasie i jej czas pojawienia"""

        for i in range(np.size(self.time)):
            if (self.time[i] > time):
                ret = dict()
                ret['val'] = self.pitch[i-1]
                ret['time'] = self.time[i-1]
                return ret
        ret = dict()
        ret['val'] = self.pitch[-1]
        ret['time'] = self.time[-1]
        return ret

    def sdisp(self, stitle = "Pitch", sxlabel = "Czas [s]", sylabel = "Wysokość dźwięku [Hz]"):

        """Wyświetla wykryte wysokości dźwięków. Używać po redukcji danych"""

        plt.figure()
        plt.stem(self.time, self.pitch)
        plt.grid(True)
        plt.title(stitle)
        plt.xlabel(sxlabel)
        plt.ylabel(sylabel)

        plt.show()

    def pdisp(self, stitle = "Pitch", sxlabel = "Czas [s]", sylabel = "Wysokość dźwięku [Hz]"):

        """Wyświetla wykryte wysokości dźwięków. Używać przed redukcją danych"""
        
        plt.figure()
        plt.plot(self.time, self.pitch)
        plt.grid(True)
        plt.title(stitle)
        plt.xlabel(sxlabel)
        plt.ylabel(sylabel)

        plt.show()
