from PyQt5.QtWidgets import QTabWidget
from qt.tabs.TabBasic import TabBasic
from qt.tabs.TabAdvanced import TabAdvanced
from qt.tabs.TabDesc import TabDesc

class TabWidget(QTabWidget):

    """
    Widget zarządzający zakładkami

    Tworzy zakładki:
      - Konwerter - pozwala na wczytanie pliku z dysku i konwersję
      - Utwór - pozwala na zmianę metrum, klucza muzycznego, dodanie autora,
      wymuszenie przeniesienia oktaw.
      - Zaawansowane - pozwala wyłączyć generowanie pliku wyjściowego,
      przełączyć tryby konwersji lub dokonać zmian w działaniu systemu.
    """

    def __init__(self, parent):
        super().__init__(parent);

        self.addTab(TabBasic(self), 'Konwerter')
        self.addTab(TabDesc(self), 'Utwór')
        self.addTab(TabAdvanced(self), 'Zaawansowane')
