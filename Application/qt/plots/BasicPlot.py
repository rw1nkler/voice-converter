from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QSizePolicy

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as pltFigureCanvas
from matplotlib.figure import Figure as pltFigure
import matplotlib.pyplot as plt

import var.variables as var

import random

class BasicPlot(pltFigureCanvas):

    """Obiekt odpowiedzialny za narysowanie obrazkka w zakładce KONWERTER"""

    _linewidth = 0.1
    _axisEnable = 'off'
    _x = None;
    _y = None;

    def __init__(self, parent = None, width = 10, height = 10, dpi = 100):
        self._fig = pltFigure(figsize = (width, height), dpi = dpi)

        pltFigureCanvas.__init__(self, self._fig)
        self.setParent(parent)

        pltFigureCanvas.setSizePolicy(self, QSizePolicy.Expanding, QSizePolicy.Expanding)
        pltFigureCanvas.updateGeometry(self)

    def songLoaded(self):
        """Rysyje obrazek po załadowaniu pliku"""

        channels = var.songHandler.getAllChannelsWithTimestamp()
        self._y = channels["Channel1"]
        self._x = channels["time"]

        self.plot()

    def plot(self):
        """Funkcja rysująca"""

        self._fig.clf()

        self._ax = self.figure.subplots()
        self._ax.axis(self._axisEnable)
        self._ax.plot(self._x, self._y, linewidth = self._linewidth)

        self.draw()
