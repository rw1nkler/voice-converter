from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QTabWidget
from PyQt5.QtWidgets import QAction
from PyQt5.QtWidgets import qApp
from PyQt5.QtCore import Qt
from qt.TabWidget import TabWidget
from var.SongHandler import SongHandler

import var.variables as var

class MainWindow(QMainWindow):

    """
    Klasa głównego okna aplikacji

    Tworzy zakładki, akcje dla menu, zarządza paskiem statusu.
    Dostęp do paska statusu odbywa się przez statyczne pole.
    """

    status_bar = None

    def __init__(self):
        super().__init__()

        self._tab_widget = None
        self._menu_bar = None
        self._file_menu = None
        self._action_close = None

        self._init_UI()
        self._create_tabs();
        self._create_actions()
        self._create_menus()

    def _init_UI(self):
        self.setWindowTitle('Music Sheet')
        self.statusBar().showMessage('Ready')
        MainWindow.status_bar = self.statusBar()

        self.resize(550, 300)

    def _create_tabs(self):
        self._tab_widget = TabWidget(self)
        self.setCentralWidget(self._tab_widget)

    def _create_actions(self):
        self._create_actions_close()

    def _create_actions_close(self):
        self._action_close = QAction('&Zamknij', self)
        self._action_close.setShortcut('Ctrl+Q')
        self._action_close.setStatusTip('Exit')
        self._action_close.triggered.connect(qApp.quit)

    def _create_menus(self):
         self._menu_bar = self.menuBar()

         self._file_menu = self._menu_bar.addMenu('&Plik')
         self._file_menu.addAction(self._action_close)

         self._helpMenu = self._menu_bar.addMenu('P&omoc')

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Q and event.modifiers() == Qt.ControlModifier:
            exit()
