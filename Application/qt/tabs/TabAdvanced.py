from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QGroupBox
from PyQt5.QtWidgets import QGridLayout
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QComboBox
from PyQt5.QtWidgets import QCheckBox
from PyQt5.QtWidgets import QLabel
from PyQt5.QtWidgets import QRadioButton

from qt.tabs.TabBasic import TabBasic
from dsp.Collector import Collector

class TabAdvanced(QWidget):

    """
    Zakładka z zaawansowanymi ustawieniami konwertera

    Umożliwia:
    - wybór trybu konwersji (relatywny/medianowy)
    - włączenie opcji, która pominie tworzenie pliku wynikowego
    - pominięcie danych pochodzących z bloku wykrywania początków dźwięków w konwersji
    - pominięcie wykrywania pauz w konwersji
    """

    def __init__(self, parent):

        self._gbox_simplify = None
        self._gbox_simplify_title = "Uprość"

        self._gbox_options = None
        self._gbox_options_title = "Konwersja"

        self._gbox_disp = None
        self._gbox_disp_title = "Wyświetlanie"

        super().__init__(parent)

        self.create_simplify_groupbox()
        self.create_options_groupbox()
        self.create_disp_groupbox()

        grid_layout = QGridLayout(self)
        grid_layout.addWidget(self._gbox_options, 0, 0, 1, 1)
        grid_layout.addWidget(self._gbox_disp, 0, 1, 1, 1)
        grid_layout.addWidget(self._gbox_simplify, 1, 0, 1, 2)

    def create_options_groupbox(self):
        """Tworzy groupbox z wyborem trybu konwersji"""

        self._gbox_options = QGroupBox(self)
        self._gbox_options.setTitle(self._gbox_options_title)

        radio1 = QRadioButton("Relatywny", self._gbox_options)
        radio2 = QRadioButton("Medianowy", self._gbox_options)

        radio1.clicked.connect(self.slot_relative)
        radio2.clicked.connect(self.slot_median)

        radio2.setChecked(True)

        vbox_layout = QVBoxLayout()
        vbox_layout.addWidget(radio1)
        vbox_layout.addWidget(radio2)

        self._gbox_options.setLayout(vbox_layout)
        self._gbox_options.adjustSize()

    def create_simplify_groupbox(self):
        """Tworzy groupbox z opcjami pominięcia niektórych kroków konwersji"""

        self._gbox_simplify = QGroupBox(self)
        self._gbox_simplify.setTitle(self._gbox_simplify_title)

        checkbox1 = QCheckBox("Konwertuj tylko na podstawie wysokości dźwięku", self._gbox_simplify)
        checkbox1.stateChanged.connect(self.slot_simplify)

        checkbox2 = QCheckBox("Nie uwzględniaj pauz", self._gbox_simplify)
        checkbox2.stateChanged.connect(self.slot_pauses)

        vbox_layout = QVBoxLayout()
        vbox_layout.addWidget(checkbox1)
        vbox_layout.addWidget(checkbox2)

        self._gbox_simplify.setLayout(vbox_layout)
        self._gbox_simplify.adjustSize()

    def create_disp_groupbox(self):
        """Tworzy groupbox z wyborem opcji pominięcia tworzenia pliku wynikowego"""

        self._gbox_disp = QGroupBox(self)
        self._gbox_disp.setTitle(self._gbox_disp_title)

        checkbox = QCheckBox("Tylko wyświetlanie", self._gbox_disp)
        checkbox.stateChanged.connect(self.slot_disp)

        vbox_layout = QVBoxLayout()
        vbox_layout.addWidget(checkbox)

        self._gbox_disp.setLayout(vbox_layout)
        self._gbox_disp.adjustSize()

    def slot_simplify(self, val):
        """Obsługa wyboru opcji pominięcia danych z bloku wykrywania początków dźwięków"""
        TabBasic.simplify = val

    def slot_pauses(slot, val):
        """Obsługa wyboru opcji pominięcia pauz"""
        Collector.pauses = val

    def slot_disp(self, val):
        """Obsługa wyboru opcji pominięcia tworzenia pliku wynikowego"""
        TabBasic.only_disp = val

    def slot_relative(self, checked):
        """Obsługa opcji zmiany trybu konwersji na relatywny"""
        Collector.conversion_mode = "Relative"

    def slot_median(self, checked):
        """Obsługa opcji zmiany trybu konwersji na medianowy"""
        Collector.conversion_mode = "Median"
