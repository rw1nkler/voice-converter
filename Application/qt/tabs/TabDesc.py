from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QGroupBox
from PyQt5.QtWidgets import QGridLayout
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QComboBox
from PyQt5.QtWidgets import QCheckBox
from PyQt5.QtWidgets import QLabel
from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtWidgets import QRadioButton

from dsp.Collector import Collector

class TabDesc(QWidget):

    """
    Zakładka umożliwiająca dokonanie wyboru parametrów generowanego zapisu

    Pozwala na zaznaczenie dodatkowych opcji dla generowanej partutury:
    - Wybór metrum
    - Wybór klucza muzycznego
    - Przeniesienie zapisu oktawę niżej
    - Uwzględnienie informacji o tempie utworu
    - Dodanie nazwy utworu oraz autora
    """

    def __init__(self, parent):
        self._gbox_desc = None
        self._gbox_desc_title = "Opis"

        self._gbox_options = None
        self._gbox_options_title = "Tempo"

        self._gbox_clef = None
        self._gbox_clef_title = "Klucz"

        self._gbox_clef8 = None
        self._gbox_clef8_title = "Przenośnik oktawowy"

        self._gbox_meter = None
        self._gbox_meter_title = "Metrum"

        super().__init__(parent)

        self.create_desc_groupbox()
        self.create_options_groupbox()
        self.create_meter_groupbox()
        self.create_clef_groupbox()
        self.create_clef8_groupbox()

        grid_layout = QGridLayout(self)
        grid_layout.addWidget(self._gbox_meter, 0, 0, 2, 1)
        grid_layout.addWidget(self._gbox_clef, 0, 1, 2, 1)
        grid_layout.addWidget(self._gbox_clef8, 0, 2, 1, 1)
        grid_layout.addWidget(self._gbox_options, 1, 2, 1, 1)
        grid_layout.addWidget(self._gbox_desc, 3, 0, 1, 3)

    def create_desc_groupbox(self):
        """Tworzy groupbox z opcjami wskazania tytułu i autora"""

        self._gbox_desc = QGroupBox(self)
        self._gbox_desc.setTitle(self._gbox_desc_title)

        title_label = QLabel("Tytuł:", self._gbox_desc)

        line_edit_title = QLineEdit(self._gbox_desc)
        line_edit_title.textChanged.connect(self.slot_title_changed)

        author_label = QLabel("Autor: ", self._gbox_desc)

        line_edit_author = QLineEdit(self._gbox_desc)
        line_edit_author.textChanged.connect(self.slot_author_changed)

        vbox_layout = QVBoxLayout()

        vbox_layout.addWidget(title_label)
        vbox_layout.addWidget(line_edit_title)

        vbox_layout.addWidget(author_label)
        vbox_layout.addWidget(line_edit_author)

        self._gbox_desc.setLayout(vbox_layout)
        self._gbox_desc.adjustSize()

    def create_options_groupbox(self):
        """Tworzy groupbox z opcją dołączenia infoprmacji o tempie"""

        self._gbox_options = QGroupBox(self)
        self._gbox_options.setTitle(self._gbox_options_title)

        vbox_layout = QVBoxLayout()

        checkbox1 = QCheckBox("Dodaj tempo", self._gbox_options)
        checkbox1.setChecked(True)
        checkbox1.stateChanged.connect(self.slot_tempo)

        vbox_layout.addWidget(checkbox1)

        self._gbox_options.setLayout(vbox_layout)
        self._gbox_options.adjustSize()

    def create_clef8_groupbox(self):
        """Tworzy groupbox z opcją wybrania przeniesienia zapisu oktawę niżej"""

        self._gbox_clef8 = QGroupBox(self)
        self._gbox_clef8.setTitle(self._gbox_clef8_title)

        checkbox = QCheckBox("Oktawę w dół", self._gbox_clef8)
        checkbox.stateChanged.connect(self.slot_clef8)

        vbox_layout = QVBoxLayout()
        vbox_layout.addWidget(checkbox)

        self._gbox_clef8.setLayout(vbox_layout)
        self._gbox_clef8.adjustSize()

    def create_clef_groupbox(self):
        """Tworzy groupbox z opcjami wyboru klucza muzycznego"""

        self._gbox_clef = QGroupBox(self)
        self._gbox_clef.setTitle(self._gbox_clef_title)

        vbox_layout = QVBoxLayout()

        radio1 = QRadioButton("Wiolinowy", self._gbox_clef)
        radio2 = QRadioButton("Basowy", self._gbox_clef)
        radio3 = QRadioButton("Altowy", self._gbox_clef)

        radio1.clicked.connect(self.slot_trebleclef)
        radio2.clicked.connect(self.slot_bassclef)
        radio3.clicked.connect(self.slot_altoclef)

        radio2.setChecked(True)

        vbox_layout.addWidget(radio1)
        vbox_layout.addWidget(radio2)
        vbox_layout.addWidget(radio3)

        self._gbox_clef.setLayout(vbox_layout)
        self._gbox_clef.adjustSize()

    def create_meter_groupbox(self):
        """Tworzy groupbox z opcją wyboru metrum"""

        self._gbox_meter = QGroupBox(self)
        self._gbox_meter.setTitle(self._gbox_meter_title)

        vbox_layout = QVBoxLayout()

        num_label = QLabel("Górna cyfra", self._gbox_meter)
        combo_num = QComboBox(self._gbox_meter)
        combo_num.addItem("1")
        combo_num.addItem("2")
        combo_num.addItem("3")
        combo_num.addItem("4")
        combo_num.addItem("5")
        combo_num.addItem("6")
        combo_num.addItem("7")
        combo_num.addItem("8")
        combo_num.addItem("9")
        combo_num.addItem("10")
        combo_num.addItem("11")
        combo_num.addItem("12")

        combo_num.setCurrentIndex(3)
        combo_num.currentIndexChanged.connect(self.slot_combo_num)

        den_label = QLabel("Dolna cyfra", self._gbox_meter)
        combo_den = QComboBox(self._gbox_meter)
        combo_den.addItem("1")
        combo_den.addItem("2")
        combo_den.addItem("4")
        combo_den.addItem("8")

        combo_den.setCurrentIndex(2)
        combo_den.currentIndexChanged.connect(self.slot_combo_den)

        vbox_layout.addWidget(num_label)
        vbox_layout.addWidget(combo_num)
        vbox_layout.addWidget(den_label)
        vbox_layout.addWidget(combo_den)

        self._gbox_meter.setLayout(vbox_layout)
        self._gbox_meter.adjustSize()


    def slot_title_changed(self, text):
        """Obsługa wpisywania tytułu utworu"""
        Collector.title = text

    def slot_author_changed(slot, text):
        """Obsługa wpisywania autora utworu"""
        Collector.author = text

    def slot_tempo(slot, val):
        """Obsługa opcji uwzględnienia tempa w zapisie utworu"""
        Collector.tempo = val

    def slot_clef8(slot, val):
        """Obsługa opcji zapisu utworu oktawę niżej"""
        Collector.clef8 = val

    def slot_combo_num(self, text):
        """Obsługa wyboru górnej liczby metrum"""
        Collector.meter_numerator = int(text) + 1

    def slot_combo_den(self, text):
        """Obsługa wyboru dolnej liczby metrum"""
        Collector.meter_denominator = 2**(int(text))

    def slot_trebleclef(self, checked):
        """Obsługa wyboru klucza wiolinowego"""
        Collector.clef = "TrebleClef"

    def slot_bassclef(self, checked):
        """Obsługa wyboru klucza basowego"""
        Collector.clef = "BassClef"

    def slot_altoclef(self, checked):
        """Obsługa wyboru klucza altowego"""
        Collector.clef = "AltoClef"
