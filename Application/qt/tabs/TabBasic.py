from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QGroupBox
from PyQt5.QtWidgets import QGridLayout
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QHBoxLayout
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtWidgets import QSizePolicy
from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtWidgets import QComboBox
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import QObject
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPalette

from dsp.Pitch import Pitch
from dsp.Onset import Onset
from dsp.Voiced import Voiced
from dsp.Tempo import Tempo
from dsp.Collector import Collector
from dsp.Filter import Filter
from var.SongHandler import SongHandler
from qt.plots.BasicPlot import BasicPlot

import var.variables as var
from scipy.io.wavfile import write

class TabBasicSignals(QObject):

    """Obiekt zajmujący się generowaniem customowych sygnałów"""
    sSongLoaded = pyqtSignal()

class TabBasic(QWidget):

    """
    Główna zakładka konwertera

    Odpowiada za wczytanie pliku z dysku, wyświetlenie grafiki sygnalizującej wczytanie,
    wybór formatu wyjściowego, wskazanie miejsca utworzenia pliku wynikowego i przeprowadzenie konwersji.
    """

    simplify = 0
    only_disp = 0

    def __init__(self, parent):

        self._gboxFigure = None
        self._gboxFigureTitle = 'Utwór'

        self._gboxLoad = None
        self._gboxLoadTitle = 'Wczytaj'
        self._gboxLoadButtonText = 'Wybierz'

        self._gboxConvert = None
        self._gboxConvertTitle = 'Konwertuj'

        self._fileDialog = None
        self._save_file_dialog = None

        self._matPlotCanva = None

        self._lineEdit = None
        self._loadButton = None
        self._comboBox = None

        self._signalsHandler = None


        self._loadBtnState = "Wybierz"
        self._lineEditRedBackground   = "background: #EC7676"
        self._lineEditWhiteBackground = "background: white"

        self._tmp_file = "song.tmp"

        super().__init__(parent)

        self.createFigureGroupbox()
        self.createUploadGroupbox()
        self.createConvertGroupbox()

        self._signalsHandler = TabBasicSignals()
        self._signalsHandler.sSongLoaded.connect(self._matPlotCanva.songLoaded)

        gridLayout = QGridLayout(self)
        gridLayout.addWidget(self._gboxFigure, 0, 0, 2, 4)
        gridLayout.addWidget(self._gboxLoad, 2, 0, 1, 2)
        gridLayout.addWidget(self._gboxConvert, 2, 2, 1, 2)

    def createFigureGroupbox(self):
        """Tworzy groupbox, w którym pojawi się grafika wczytanego utworu"""

        self._gboxFigure = QGroupBox(self)
        self._gboxFigure.setTitle(self._gboxFigureTitle)

        self._matPlotCanva = BasicPlot(self._gboxFigure)

        vBoxLayout = QVBoxLayout()
        vBoxLayout.addWidget(self._matPlotCanva)

        self._gboxFigure.setLayout(vBoxLayout)
        self._gboxFigure.adjustSize()

    def createUploadGroupbox(self):
        """Tworzy groupbox odpowiedzialny za wczytanie pliku audio"""

        self._gboxLoad = QGroupBox(self)
        self._gboxLoad.setTitle(self._gboxLoadTitle)

        self._lineEdit = QLineEdit(self._gboxLoad)
        self._lineEdit.textChanged.connect(self.slot_lineEditChanged)

        self._loadButton = QPushButton(self._gboxLoadButtonText, self._gboxLoad)
        self._loadButton.clicked.connect(self.slot_loadButtonClicked)

        hBoxLayout = QHBoxLayout()
        hBoxLayout.addWidget(self._lineEdit)
        hBoxLayout.addWidget(self._loadButton)

        self._gboxLoad.setLayout(hBoxLayout)
        self._gboxLoad.adjustSize()

    def createConvertGroupbox(self):
        """Tworzy groupbox, w którym znajdują się widgety pozwalające na wskazanie
        formatu wyjściowego i rozpoczęcie konwersji"""

        self._gboxConvert = QGroupBox(self)
        self._gboxConvert.setTitle(self._gboxConvertTitle)

        self._comboBox = QComboBox(self._gboxConvert)
        self._comboBox.addItem("MusicXML", 1)
        self._comboBox.addItem("MIDI", 2)

        self._convertBtn = QPushButton("Konwertuj", self._gboxConvert)
        self._convertBtn.setEnabled(False)
        self._convertBtn.clicked.connect(self.slot_convertBtnClicked)

        gridLayout = QGridLayout()
        gridLayout.addWidget(self._comboBox, 0, 0, 1, 2);
        gridLayout.addWidget(self._convertBtn, 0, 2, 1, 1);

        self._gboxConvert.setLayout(gridLayout)
        self._gboxConvert.adjustSize()

    def slot_loadButtonClicked(self):
        """Odpowiada za reakcję na naciśnięcie przycisku WCZYTAJ"""

        self.clearWrongPathToSongErr()
        if self._loadBtnState == 'Wybierz':
            self._fileDialog = QFileDialog()
            self._fileDialog.setFileMode(QFileDialog.ExistingFile)
            self._fileDialog.setNameFilter("*.wav")
            if self._fileDialog.exec_():
                self._fileName = self._fileDialog.selectedFiles()
                self._fileName = self._fileName[0]
            else:
                self._fileName = "brak"

        elif self._loadBtnState == 'Wczytaj':
            self_.fileName = self._lineEdit.text()

        handler = SongHandler(self._fileName)
        var.songHandler = handler

        try :
          var.songHandler.load()
        except FileNotFoundError:
          self.wrongPathToSongErr()
        else:
          self._convertBtn.setEnabled(True)
          self._signalsHandler.sSongLoaded.emit()

    def slot_convertBtnClicked(self):
        """Odpowiada za reakcję na naciśnięcie przycisku KONWERTUJ (konwersja)"""

        convert = False
        if TabBasic.only_disp == 0:

            self._save_file_dialog = QFileDialog()
            self._save_file_dialog.setFileMode(QFileDialog.AnyFile)

            if self._comboBox.currentIndex() == 0:
                format = "musicxml"
                self._save_file_dialog.setNameFilter("*.xml")
            else:
                format = "midi"
                self._save_file_dialog.setNameFilter("*.midi")

            if self._save_file_dialog.exec_():
                self._save_file_name = self._save_file_dialog.selectedFiles()
                self._save_file_name = self._save_file_name[0]

                convert = True
            else:
                self._save_file_name = "None"
                convert = False

        else:
            format = "disp"
            convert = True
            self._save_file_name = "None"

        if (convert == True):

            channels = var.songHandler.getAllChannelsWithTimestamp()
            values = channels["Channel1"]

            #
            # Tutaj zaczyna się konwertowanie nagrania
            #

            # Filtr środkowoprzepustowy

            filt = Filter(values, 44100)
            filtered = filt.fir()
            write(self._tmp_file, 44100, filtered)


            # Blok wykrywania aktywności mówcy

            voiced = Voiced(44100)
            # voiced.pdisp()
            voiced.filter()
            #voiced.sdisp()


            # Blok wykrywania wysokości dźwięku

            pitch = Pitch(self._tmp_file, 44100)
            # pitch.pdisp("Wynik algorytmu Yin")
            pitch.exclude_unvoiced(voiced)
            # pitch.pdisp("Wykluczenie bezgłośnych fragmentów")
            pitch.filter()
            pitch.compensate_fir_filter(200)


            # Blok wykrywania początków dźwięków

            onset = Onset(self._fileName, 44100)
            # onset.disp()
            # onset.disp()


            # Wykrywanie tempa utworu

            tempo = Tempo(self._fileName, 44100)


            # Blok decyzyjny

            collector = Collector(pitch, onset, voiced, tempo)
            

            if TabBasic.simplify != 0:
                collector.convert(format, self._save_file_name, mode="Pitch")
            else:
                collector.convert(format, self._save_file_name)

    def slot_lineEditChanged(self, text):
        """Obsługa sygnału sygnalizującego zmianę edita ze ścieżką do pliku"""

        self.clearWrongPathToSongErr()

        if text != '':
            self._loadBtnState = 'Wczytaj'
            self._loadButton.setText('Wczytaj')
            var.statusBar.showMessage('Ready to load')
        else :
            self._loadBtnState = 'Wybierz'
            self._loadButton.setText('Wybierz')
            var.statusBar.showMessage('Ready')

    def wrongPathToSongErr(self):
        """Reakcja na podanie błędnej ścieżki do pliku """
        self._lineEdit.setStyleSheet(self._lineEditRedBackground)

    def clearWrongPathToSongErr(self):
        """Cofnięcie reakcji na podanie błędnej ścieżki do pliku"""
        self._lineEdit.setStyleSheet(self._lineEditWhiteBackground)
