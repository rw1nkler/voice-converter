#!/home/robert/anaconda3/bin/python3

import sys
from PyQt5.QtWidgets import QApplication
from qt.MainWindow import MainWindow

import var.variables as var

if __name__ == '__main__':
    var.init()

    app = QApplication(sys.argv)

    MyWindow = MainWindow()
    MyWindow.show()

    sys.exit(app.exec_())
