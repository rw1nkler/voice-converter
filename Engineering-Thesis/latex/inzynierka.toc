\contentsline {chapter}{Wst\IeC {\k e}p}{2}
\contentsline {section}{Zadanie postawione w pracy}{2}
\contentsline {section}{Za\IeC {\l }o\IeC {\.z}enia}{2}
\contentsline {section}{Organizacja dokumentu}{2}
\contentsline {chapter}{\numberline {1}Wybrane zagadnienia teorii muzyki}{3}
\contentsline {section}{\numberline {1.1}Odleg\IeC {\l }o\IeC {\'s}ci mi\IeC {\k e}dzy d\IeC {\'z}wi\IeC {\k e}kami}{3}
\contentsline {subsection}{\numberline {1.1.1}Skala pitagorejska}{3}
\contentsline {subsection}{\numberline {1.1.2}Str\IeC {\'o}j r\IeC {\'o}wnomiernie temperowany}{3}
\contentsline {section}{\numberline {1.2}Pi\IeC {\k e}ciolinia i nazwy d\IeC {\'z}wi\IeC {\k e}k\IeC {\'o}w}{3}
\contentsline {section}{\numberline {1.3}Nazwy oktaw}{4}
\contentsline {section}{\numberline {1.4}Klucze muzyczne}{4}
\contentsline {section}{\numberline {1.5}Interwa\IeC {\l }y}{5}
\contentsline {section}{\numberline {1.6}Znaki chromatyczne}{5}
\contentsline {section}{\numberline {1.7}Enharmonia}{5}
\contentsline {section}{\numberline {1.8}Skala muzyczna}{6}
\contentsline {section}{\numberline {1.9}Gama}{6}
\contentsline {section}{\numberline {1.10}Tonacja i znaki przykluczowe}{6}
\contentsline {section}{\numberline {1.11}Czasy trwania nut i pauz}{6}
\contentsline {section}{\numberline {1.12}Podzia\IeC {\l } taktowy i metrum}{7}
\contentsline {section}{\numberline {1.13}Skale d\IeC {\'z}wi\IeC {\k e}k\IeC {\'o}w w uk\IeC {\l }adzie czterog\IeC {\l }osowym}{7}
\contentsline {chapter}{\numberline {2}Konwersja \IeC {\'s}piewu do notacji muzycznej}{8}
\contentsline {section}{\numberline {2.1}Badane parametry d\IeC {\'z}wi\IeC {\k e}ku}{8}
\contentsline {section}{\numberline {2.2}Przyk\IeC {\l }ady system\IeC {\'o}w ATM}{8}
\contentsline {subsection}{\numberline {2.2.1}Transkrypcja muzyki monofonicznej}{8}
\contentsline {subsection}{\numberline {2.2.2}Transkrypcja muzyki polifonicznej}{9}
\contentsline {subsection}{\numberline {2.2.3}Uog\IeC {\'o}lniona struktura systemu ATM}{11}
\contentsline {chapter}{\numberline {3}Algorytmy}{12}
\contentsline {section}{\numberline {3.1}Wykrywanie wysoko\IeC {\'s}ci d\IeC {\'z}wi\IeC {\k e}ku}{12}
\contentsline {subsection}{\numberline {3.1.1}Autokorelacja (ACF)}{12}
\contentsline {subsection}{\numberline {3.1.2}Average Magnitude Difference Function (AMDF)}{12}
\contentsline {subsection}{\numberline {3.1.3}Algorytm Yin}{12}
\contentsline {subsubsection}{Krok 1 - Obliczenie warto\IeC {\'s}ci SDF}{13}
\contentsline {subsubsection}{Krok 2 - Obliczenie warto\IeC {\'s}ci CMNDF}{13}
\contentsline {subsubsection}{Krok 3 - Ustawienie globalnego progu}{13}
\contentsline {subsubsection}{Krok 4 - Interpolacja paraboliczna}{13}
\contentsline {subsection}{\numberline {3.1.4}Kr\IeC {\'o}tkoczasowa transformata Fouriera (STFT)}{13}
\contentsline {subsubsection}{Dob\IeC {\'o}r szeroko\IeC {\'s}ci okna}{13}
\contentsline {subsubsection}{Dob\IeC {\'o}r kszta\IeC {\l }tu okna}{13}
\contentsline {subsubsection}{Uzupe\IeC {\l }nianie zerami}{14}
\contentsline {section}{\numberline {3.2}Wykrywanie pocz\IeC {\k a}tk\IeC {\'o}w nut}{14}
\contentsline {subsection}{\numberline {3.2.1}Wa\IeC {\.z}enie energii sygna\IeC {\l }u}{14}
\contentsline {subsection}{\numberline {3.2.2}\textit {Beat spectrum}}{14}
\contentsline {subsubsection}{Krok 1 - Parametryzacja}{14}
\contentsline {subsubsection}{Krok 2 - Utworzenie macierzy podobie\IeC {\'n}stwa}{14}
\contentsline {subsubsection}{Krok 3 - Utworzenie \textit {beat spectrum}}{15}
\contentsline {subsubsection}{Dodatkowy Krok 4 - Wyznaczenie pocz\IeC {\k a}tk\IeC {\'o}w d\IeC {\'z}wi\IeC {\k e}k\IeC {\'o}w}{15}
\contentsline {section}{\numberline {3.3}Detekcja aktywno\IeC {\'s}ci m\IeC {\'o}wcy}{15}
\contentsline {subsection}{\numberline {3.3.1}Detekcja na podstawie amplitudy sygna\IeC {\l }u}{15}
\contentsline {subsection}{\numberline {3.3.2}Detekcja na podstawie energii sygna\IeC {\l }u}{15}
\contentsline {section}{\numberline {3.4}Wykrywanie tempa utworu}{16}
\contentsline {subsection}{\numberline {3.4.1}Sumowanie funkcji autokowariancji}{16}
\contentsline {subsubsection}{Krok 1 - Podzia\IeC {\l } na zakresy}{16}
\contentsline {subsubsection}{Krok 2 - Wyznaczenie pocz\IeC {\k a}tk\IeC {\'o}w nut}{16}
\contentsline {subsubsection}{Krok 3 - Wyznaczenie tempa}{16}
\contentsline {section}{\numberline {3.5}Wykrywanie tonacji utworu}{16}
\contentsline {chapter}{\numberline {4}Budowa stworzonego systemu ATM}{18}
\contentsline {section}{\numberline {4.1}Og\IeC {\'o}lna budowa systemu}{18}
\contentsline {section}{\numberline {4.2}Blok detekcji aktywno\IeC {\'s}ci m\IeC {\'o}wcy}{18}
\contentsline {section}{\numberline {4.3}Blok wykrywania wysoko\IeC {\'s}ci d\IeC {\'z}wi\IeC {\k e}ku}{18}
\contentsline {subsection}{\numberline {4.3.1}Wst\IeC {\k e}pna filtracja sygna\IeC {\l }u}{18}
\contentsline {subsection}{\numberline {4.3.2}Analiza wysoko\IeC {\'s}ci d\IeC {\'z}wi\IeC {\k e}ku}{19}
\contentsline {section}{\numberline {4.4}Blok wykrywania pocz\IeC {\k a}tk\IeC {\'o}w d\IeC {\'z}wi\IeC {\k e}k\IeC {\'o}w}{21}
\contentsline {section}{\numberline {4.5}Blok wykrywania tempa utworu}{22}
\contentsline {section}{\numberline {4.6}Blok decyzyjny}{22}
\contentsline {subsection}{\numberline {4.6.1}Weryfikacja wykrytych pocz\IeC {\k a}tk\IeC {\'o}w nut}{22}
\contentsline {subsection}{\numberline {4.6.2}Analiza wysoko\IeC {\'s}ci nut}{23}
\contentsline {subsubsection}{Tryb relacyjny}{23}
\contentsline {subsubsection}{Tryb medianowy}{23}
\contentsline {subsection}{\numberline {4.6.3}Analiza czasu trwania nut}{24}
\contentsline {subsection}{\numberline {4.6.4}Uwzgl\IeC {\k e}dnienie pauz}{25}
\contentsline {subsection}{\numberline {4.6.5}Wyznaczenie znak\IeC {\'o}w przykluczowych}{25}
\contentsline {subsection}{\numberline {4.6.6}Dodanie pozosta\IeC {\l }ych informacji do partytury}{25}
\contentsline {chapter}{\numberline {5}Opis aplikacji i wykorzystane technologie}{26}
\contentsline {section}{\numberline {5.1}Wykorzystane technologie}{26}
\contentsline {subsubsection}{PyQt5}{26}
\contentsline {subsubsection}{music21}{26}
\contentsline {subsubsection}{scipy, numpy i matplotlib}{26}
\contentsline {section}{\numberline {5.2}Interfejs graficzny}{26}
\contentsline {chapter}{\numberline {6}Testy aplikacji}{28}
\contentsline {section}{\numberline {6.1}Nagrania testowe}{28}
\contentsline {section}{\numberline {6.2}Blok wykrywania wysoko\IeC {\'s}ci d\IeC {\'z}wi\IeC {\k e}ku}{28}
\contentsline {section}{\numberline {6.3}Blok wykrywania pocz\IeC {\k a}tk\IeC {\'o}w d\IeC {\'z}wi\IeC {\k e}k\IeC {\'o}w}{29}
\contentsline {section}{\numberline {6.4}Blok wykrywania tempa utworu}{29}
\contentsline {section}{\numberline {6.5}Blok wykrywania aktywno\IeC {\'s}ci m\IeC {\'o}wcy}{29}
\contentsline {section}{\numberline {6.6}Ocena systemu - blok decyzyjny}{30}
\contentsline {section}{\numberline {6.7}Ocena jako\IeC {\'s}ciowa wykonanych transkrypcji}{31}
\contentsline {chapter}{Podsumowanie}{33}
\contentsline {chapter}{\numberline {A}Za\IeC {\l }\IeC {\k a}czniki}{34}
\contentsline {subsubsection}{Dodatkowe}{34}
\contentsline {section}{\numberline {A.1}Testy systemu - Panie Janie (sylaba ,,pa'')}{35}
\contentsline {section}{\numberline {A.2}Testy systemu - Panie Janie (tekst)}{37}
\contentsline {section}{\numberline {A.3}Testy systemu - Oda do rado\IeC {\'s}ci (sylaba ,,pa'')}{39}
\contentsline {section}{\numberline {A.4}Testy systemu - Oda do rado\IeC {\'s}ci (tekst)}{42}
\contentsline {section}{\numberline {A.5}Wzorcowe transkrypcje}{44}
