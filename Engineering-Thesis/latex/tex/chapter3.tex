\chapter{Konwersja śpiewu do notacji muzycznej}
%\vskip 25px


Konwersja śpiewu pojedynczej osoby do zapisu nutowego jest związana z zagadnieniem określanym jako automatyczna transkrypcja muzyki (ATM). Zadanie to polega na przetworzeniu muzycznego sygnału akustycznego do wybranej formy notacji muzycznej \cite{BEN13-ATM}. Jedną z tych form może być np. partytura lub tabulatura, jak również cyfrowe formaty MIDI \cite{MIDI-spec} bądź musicXML \cite{GOOD01-musicXML}. 

Automatyczna transkrypcja muzyki, w ogólnym przypadku, dotyczy sporządzenia zapisu utworu polifonicznego, na który mogą składać się zarówno instrumenty generujące tzw.~dźwięki muzyczne takie jak pianino, flet czy gitara, jak również instrumenty perkusyjne. Dodatkowo w utworze mogą występować soliści, kilku wokalistów a nawet orkiestra \cite{PLUMBLEY02-ATM}. Tak postawione zadanie jest dużo trudniejsze od analizy pojedynczej linii melodycznej i doczekało się wielu wyrafinowanych algorytmów \cite{BEN13-ATM}. Wiele z nich zostało zaprezentowanych na międzynarodowym konkursie \textit{Music Information Retrieval Evaluation Exchange (MIREX)}, który dotyczy m.in. zagadnienia wyodrębniania melodii z nagrania \cite{POLINER07-MelodyEvaluation}. Dzięki temu zestawieniu, można dokonać porównania najbardziej obiecujących algorytmów i  wysnuć wnioski na temat projektowania systemów automatycznej transkrypcji muzyki również dla przypadku jednogłosowego. 

\section{Badane parametry dźwięku}

Aby zaprojektować system konwersji śpiewu na zapis nutowy, należy najpierw zastanowić się, jakie własności dźwięku trzeba wyznaczyć. Zasadniczo można wyróżnić trzy podstawowe parametry, które są istotne dla wykonania transkrypcji utworu \cite{BELLO00-Techniques}:

	\begin{itemize}
		\item Wysokość dźwięku
		\item Czas rozpoczęcia się dźwięku
		\item Czas trwania dźwięku
	\end{itemize}
	
Pozostałe własności takie jak artykulacja czy barwa mogą zostać pominięte, ponieważ są indywidualnymi cechami śpiewaka lub zależą od interpretacji dzieła muzycznego. Niemniej wyznaczenie dodatkowych parametrów może pomóc w utworzeniu bogatszego opisu utworu. Przykładem może być m.in. identyfikacja tempa oraz określenie dynamiki. Mogą one posłużyć do wyznaczenia podziału taktowego dzieła muzycznego. Z kolei analiza pozycji nuty w obrębie taktu, połączona ze znajomością jej wysokości stwarza możliwości identyfikacji tonacji. Pozyskane dodatkowe informacje mogą stanowić ważny element samego procesu identyfikacji kolejnych dźwięków. Na ich podstawie można np. zbudować system przetwarzania oparty o model probabilistyczny \cite{RYYNANEN04-Modeling}.

\section{Przykłady systemów ATM}

Wiele dostępnych prac opisuje system automatycznej transkrypcji dla muzyki polifonicznej. Dają one obraz tego, jakie metody są stosowane w celu uzyskania najlepszych rezultatów. Mimo to warto zaznajomić się również z metodami dostępnymi dla muzyki monofonicznej.

\subsection {Transkrypcja muzyki monofonicznej}

\label{subsec: 4_system_mono}

Uproszczenie problemu do przypadku, w którym w danym momencie czasu może wybrzmiewać tylko jeden dźwięk prowadzi do równoczesnego uproszczenia struktury systemu automatycznej transkrypcji. Przykładowa budowa takiego systemu została przedstawiona na kartach czasopisma \textit{Cybernetics and Systems} \cite{PLUMBLEY02-ATM}:

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{img/struktura-ATM.png}
	\caption{Diagram blokowy monofonicznego systemu transkrypcji \cite{PLUMBLEY02-ATM}.}
	\label{fig:boat1}
\end{figure}

Sygnał dźwiękowy dostający się na wejście przedstawionego systemu jest dzielony na ramki. W tym wypadku zastosowano porcje danych o długości 4096 próbek. Następnie jest on analizowany pod trzema kątami. 

Blok wykrywania wysokości dźwięku (\textit{Pitch tracker}) dokonuje identyfikacji częstotliwości sygnału akustycznego na podstawie algorytmu autokorelacyjnego. Następnie wartość ta jest klasyfikowana do jednego z przedziałów odpowiadającemu nucie o konkretnej wysokości w bloku przypisania (\textit{Pitch to MIDI}). Tak przygotowana informacja jest dostarczana do bloczka decyzyjnego (\textit{Collector}), który na jej podstawie oraz danych o początku wystąpienia dźwięku (\textit{Onset detector}) umiejscawia nutę w czasie. Z kolei informacja o czasie trwania nuty wnioskowana jest na podstawie pojawienia się kolejnego dźwięku lub spadku głośności poniżej ustalonego progu. Analizę tą przeprowadza blok obwiedni (\textit{Envelope}). Dodatkowo blok decyzyjny posiada ograniczenie, które odrzuca występowanie nut krótszych od 100ms. 

Wymieniony system prezentuje typowe rozwiązanie problemu automatycznej transkrypcji muzyki. Potwierdzenie tego, można znaleźć w pracy \cite{BEN13-ATM}, prezentującej następujące podejście:

	\begin{figure}[H]
		\centering
		\includegraphics[width=0.7\linewidth]{img/struktura-ATM3.png}
		\caption{Przykład drugiej struktury systemu ATM \cite{BEN13-ATM}.}
	\end{figure}

Podobnie jak poprzednio, wysokość dźwięku estymowana jest za pomocą autokorelacji. Różnica między wymienionymi rozwiązaniami polega na tym, że blok pomiaru wzmocnienia (\textit{Envelope}), jest odpowiedzialny nie tylko za wykrywanie momentów ciszy, ale i czasu rozpoczęcia oraz zakończenia dźwięku. Detekcja nowej nuty ma miejsce przez monitorowanie czasu trwania pojawiających się dźwięków o odpowiednio dużej energii. Jeżeli któryś z nich przekroczy założony minimalny czas trwania, zapamiętywana jest informacja o nowej nucie. W analogiczny sposób wyznaczana jest informacja o końcu dźwięku. Zebrane informacje trafiają do bloczka decyzyjnego (\textit{Collector}), który na ich podstawie dokonuje transkrypcji muzyki.

Autokorelacja jest chętnie stosowaną metodą wykrywania wysokości dźwięku ze względu na swoją prostotę. Dodatkową zaletą jest fakt niezależności rozdzielczości częstotliwościowej od długości ramki. Niestety przez swoje ograniczenia ma ograniczone zastosowanie w systemach ATM dla muzyki polifonicznej \cite{Klapuri-Thesis}.

 Inne często stosowane rozwiązania dotyczące wykrywania wysokości dźwięku to analiza Fourierowska, transformacja falkowa, transformacja constant-Q  \cite{PERTUSA10-Book}. Dodatkowe algorytmy wykorzystywane głównie w fazie postprocessingu to m.in. ukryte modele Markowa \cite{POLINER06-HMM}, a także wspomniana konstrukcja modelu np. probabilistycznego opartego o wiedzę na temat tonacji i następstw dźwięków \cite{RYYNANEN04-Modeling}.

\subsection{Transkrypcja muzyki polifonicznej}

Trudniejszym zadaniem jest transkrypcja muzyki polifonicznej. Również w tym wypadku budowa systemu jest podobna z tą różnicą, że analiza muzyki wielogłosowej wymaga bardziej rozbudowanych algorytmów przetwarzania. Między innymi z powodu harmonicznych pojawiających się w miejscach całkowitych wielokrotności częstotliwości tonu podstawowego, co dla przypadku wielogłosowego tworzy bardzo skomplikowany obraz częstotliwościowy sygnału \cite{POLINER07-MelodyEvaluation} \cite{Klapuri-Thesis} .

Rzetelne przedstawienie i porównanie systemów ATM, można znaleźć w \cite{POLINER07-MelodyEvaluation}.
Jest to artykuł prezentujący rozwiązania przedstawione na wspomnianym wcześniej międzynarodowym konkursie \textit{MIREX} w 2007 roku. W celach porównawczych, autorzy artykułu wyodrębniają pewne cechy systemu służące ukazaniu podobieństw między podejściami do rozwiązania problemu transkrypcji muzyki polifonicznej.

Kwintesencją zestawienia jest tabela prezentująca najważniejsze własności systemów. Fragment wspomnianej tabeli został przedstawiony poniżej:

 \begin{table}[H]
 	\centering
 	\begin{tabular}{ |c | c | c | c |}
 		\hline
 		System & Metoda & Post-processing & Wykrywanie aktywności  \\ \hline \hline
        \multirow{2}{*}{Dressler \cite{DRESSLER05}} & \multirow{2}{*}{|STFT| + sinus.} & reguły potoków & \multirow{2}{*}{gr. melodii + lok. próg} \\ 
        & & \textit{(Streaming rules)} & \\ \hline
        
        \multirow{2}{*}{Marolt \cite{MAROLT04}} & \multirow{2}{*}{|STFT| + sinus.} & reguły sąsiedztwa & \multirow{2}{*}{grupowanie melodii} \\
        && \textit{(Proximity rules)} & \\ \hline
        
        \multirow{2}{*}{Goto \cite{GOTO04}} & \multirow{2}{*}{kilka |STFT| + sinus.} & śledzący agenci & \multirow{2}{*}{(brak)}  \\
        && \textit{(Tracking Agents)} & \\ \hline
        
        Ryyn{\"a}nen \cite{RYYNANEN07} & modele + |STFT| & HMM & założony model \\ \hline
        
        Poliner \cite{POLINER05} & |STFT| & - & globalny próg \\ \hline
        
        \multirow{2}{*}{Paiva \cite{PAIVA04}} & \multirow{2}{*}{korelogram} & reguły przycinania & \multirow{2}{*}{grupowanie melodii} \\ 
        & & \textit{(Pruning rules)} & \\ \hline
        Vincent \cite{VINCENT05} & YIN / okna czasowe & HMM & (brak) \\ \hline
 	\end{tabular}
 	\caption{Tabela opisująca systemy ATM zaprezentowane podczas konkursu MIREX}
 \end{table}
 
 Pierwsza kolumna wskazuje na twórcę systemu razem z pracą, w której znalazł się opis wykonanego systemu.
 
 Druga kolumna zawiera informację o głównej metodzie wykrywania wysokości dźwięków w systemie ATM. Zgodnie z zamieszczonym porównaniem najczęściej wybieraną metodą jest posługiwanie się modułem krótkoczasowej transformaty Fouriera (|STFT|). Pięć na siedem prezentowanych prac korzysta z tej metody, często stosując dodatkowe elementy przetwarzania. Ważnym parametrem STFT jest długość okna, która determinuje rozdzielczość częstotliwościową wyniku \cite{OSOWSKI16}. Najkrótsze okno zastosowano w pracy Dressler i wynosiło ono 46ms. Najdłuższe okno trwające 128ms zastosował z kolei Poliner. Częstotliwość próbkowania nagrań testowych wynosiła $44,1kHz$.
 Dwie prace wykorzystały algorytmy bazujące na autokorelacji. Jedna z nich korzystała z zmodyfikowanej wersji algorytmu Yin.
 
 Interesującym elementem zestawienia jest kolejna kolumna, przedstawiająca metodę, która służyła wyodrębnieniu pojawiających się dźwięków spośród wykrytych częstotliwości. W tym zestawieniu widać większą różnorodność, jednak wiele z zaprezentowanych systemów korzysta z pewnego założonego modelu sygnału. 
 
 W przypadku systemów Dressler, Marolta i Paivy z znajdywanych częstotliwości tworzone są podzbiory dźwięków, składane następnie w linie melodyczne. Utworzone melodie są sprawdzane pod kątem jakości, m.in. przez sprawdzenie czy nie następują duże skoki w częstotliwościach dźwięków. W ten sposób oceniana jest ich poprawność. Ciekawym rozwiązanie cechuje się system Goto, który na bieżąco bada różne hipotezy dotyczące pojawiających się częstotliwości dźwięków. Konkurujące hipotezy rywalizują ze sobą, mając do dyspozycji bieżącą wysokość dźwięku i dane poprzednie. Po ocenie każdej hipotezy zostaje wybierana najkorzystniejsza spośród nich. Wysokość dźwięku przez nią wskazywana jest uważana za wykrytą. Kolejną popularną metodą jest zastosowanie ukrytych modeli Markowa (HMM).
 
 Ostatnia kolumna przedstawia w jaki sposób wykrywane są momenty obecności melodii lub jej brak. Systemy Goto i Vincenta nie stosują pod tym względem rozróżnienia. Rozwiązania Dressler, Marlota i Paivy stosując klasyfikację opartą o melodię, w pewny momencie przestaną śledzić niepasujące wyniki. Poliner stosuje prosty próg globalny.

 
 \newpage
 
\subsection{Uogólniona struktura systemu ATM}
 
 Przedstawione rozwiązania prezentują podobieństwo różnych systemów automatycznej transkrypcji muzyki.
 Uogólniony model systemu ATM może wyglądać następująco \cite{BEN13-ATM}:
	
	\begin{figure}[H]
		\includegraphics[width=\linewidth]{img/struktura-ATM2.png}
		\caption{Uogólniona struktura systemu ATM \cite{BEN13-ATM}.}
	\end{figure}
	
Opcjonalne składniki systemu zostały przedstawione na rysunku za pomocą przerywanych kresek. Miejsca w których następuje fuzja informacji i komunikacja między poszczególnymi blokami zostały oznaczone strzałkami.

