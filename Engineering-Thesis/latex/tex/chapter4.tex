\chapter{Algorytmy}
\section{Wykrywanie wysokości dźwięku}

Zasadniczo najważniejszym rodzajem algorytmów służących wykonaniu transkrypcji muzyki, są te dotyczące wykrywania wysokości dźwięku \cite{POLINER07-MelodyEvaluation}. Zagadnienie to jest związane z estymacją częstotliwości tonu podstawowego $f_0$. Problem ten jest istotnym elementem analizy sygnału mowy i w tej dziedzinie określany jest jako estymacja tonu krtaniowego \cite{MAKOWSKI11-ARM}. Wiele złożonych algorytmów określania wysokości dźwięku powstało na bazie doświadczeń pochodzących z analizy mowy.

Można wyróżnić metody estymacji tonu podstawowego oparte o przetwarzanie sygnału w dziedzinie czasu. Przykładami takich algorytmów mogą być np. metoda autokorelacyjna \cite{RABINER77-Auto}, algorytm RAPT \cite{TALKIN95-RAPT} czy też algorytm Yin \cite{CHEVEIGNE02-Yin}. Inna grupa metod estymacji fundamentalnej częstotliwości dźwięku opiera się na analizie częstotliwościowej lub quasi-częstotliwościowej, wykorzystującej m.in. krótkoczasową transformatę Fouriera (STFT) \cite{OSOWSKI16} oraz algorytmy oparte o cepstrum \cite{NOLL67-Cepstrum-Pitch}.

\subsection{Autokorelacja (ACF)}

Funkcja autokorelacji wskazuje stopień zależności sygnału od niego samego przesuniętego o dany odcinek czasu $\tau$. Dla skończonego dyskretnego sygnału długości $N$ próbek autokorelacja dana jest wzorem \cite{CUADRA01-Pitch}:

\begin{equation}
\label{ACF}
ACF(\tau) =\frac{1}{N} \sum_{n=0}^{N-1} x(n)x(n + \tau)
\end{equation}

W literaturze można też znaleźć postać autokorelacji uwzględniającą dodatkowe okno o długości $W$ próbek, którego początek znajduje się w chwili $t$ \cite{PERTUSA10-Book}. Tak zdefiniowana autokorelacja obliczana jest jedynie dla tych próbek sygnału $x(n)$, które w danej chwili czasu są objęte oknem. Często usuwa się również czynnik skalujący $\frac{1}{N}$, który nie zmienia istotnych własności przebiegu funkcji. Wzór autokorelacji z uwzględnieniem wymienionych poprawek wygląda następująco:

\begin{equation}
\label{ACF2}
ACF'(\tau) = \sum_{n=t}^{N + W-1} x(n)x(n + \tau)
\end{equation}

Zależność ta w prosty sposób pozwala wykryć okresowy sygnał. Maksimum globalne ciągu autokorelacji skończonego sygnału znajduje się w $ACF(0)$. Nieco mniejsze maksimum powinno wystąpić po upłynięciu dokładnie jednego okresu sygnału. 
Dla sygnału śpiewu widoczne będą także lokalne maksima mniejszej wysokości, które są efektem występowania harmonicznych. Tłumaczy to konieczność poszukiwania ekstremum o wartości mniejszej, lecz zbliżonej do tej z początku układu współrzędnych. 

Znając częstotliwość próbkowania sygnału $f_s$, poszukiwana częstotliwość $f_0$ dana jest wzorem:

\begin{equation}
\label{ACF-f0}
f_0 = \frac{f_s}{\tau_0}
\end{equation}

\subsection{Average Magnitude Difference Function (AMDF)}

Alternatywą dla autokorelacji w wyznaczaniu okresowości sygnału może być funkcja AMDF \cite{ROSS74-AMDF}. Dana jest ona wzorem:

\begin{equation}
\label{AMDF}
AMDF(\tau) =\frac{1}{N} \sum_{n=0}^{N-1} | x(n) - x(n + \tau) |
\end{equation}

Różnica funkcji powtarzających się w czasie powinna po przesunięciu o pełen okres równać się zeru. Dla skończonego sygnału dyskretnego w celu znalezienia częstotliwości $f_0$, należy postąpić analogicznie jak dla autokorelacji z tą różnicą, że tym razem poszukuje się minimum zbliżonego jak najbardziej do minimum globalnego. Najmniejsza wartość funkcji AMDF występuje dla $n = 0$ i równa jest zeru. Również w tym przypadku analizując sygnał śpiewu, należy mieć na uwadze składowe harmoniczne, powodujące powstawanie lokalnych minimów.

Częstotliwość $f_0$ można uzyskać z równania (\ref{ACF-f0})

\subsection{Algorytm Yin}

\label{subsec:4_yin}

Na podobnej idei do funkcji AMDF bazuje inny algorytm - Yin \cite{CHEVEIGNE02-Yin}. Został on skutecznie zastosowany w systemie transkrypcji śpiewu opracowanym przez Ryyn{\"a}nen i Klapuri \cite{RYYNANEN04-Modeling}, osiągając bardzo dobre wyniki. Poszczególne kroki algorytmu zostały przedstawione poniżej.

\subsubsection{Krok 1 - Obliczenie wartości SDF}

W celu zmniejszenia błędów algorytm Yin bazuje na zmodyfikowanej postaci \textit{Squared Difference Function} (SDF):

\begin{equation}
SDF(\tau) =\frac{1}{N} \sum_{n=0}^{N-1} ( x(n) - x(n + \tau) )^2
\end{equation}

\subsubsection{Krok 2 - Obliczenie wartości CMNDF}

Modyfikacja SDF polega na zastąpieniu wartości zerowej dla $\tau = 0$ przez wartość~$1$. Dzięki temu algorytm analizy funkcji wynikowej może być prostszy. Ponadto poza wspomnianym punktem wartość SDF dzielona jest przez średnią wyliczoną z mniejszego przedziału. Postać funkcji nazwanej \textit{Cumulative mean normalized difference function} (CMNDF), jest następująca:

\begin{equation}
\label{macierz}
CMNDF = \left\{ \begin{array}{ll}
1, & \textrm{gdy $\tau = 0$} \\
\displaystyle {\frac{SDF(\tau)}{\frac{1}{\tau}  \sum_{j=1}^{\tau} SDF(j)}} & \textrm{gdy $\tau \neq 0$} \\
\end{array}
\right.
\end{equation}

\subsubsection {Krok 3 - Ustawienie globalnego progu}

Kolejnym krokiem dostosowania algorytmu jest wybranie progu. Pierwsza wartość funkcji $\tau_d$ niższa od progu powinna zostać wybrana, jako związana ze znalezioną częstotliwością  $f_0$. W oryginalnej pracy wartość progu została wybrana jako $0,1$. Jeżeli wartość funkcji nie spada poniżej tej wartości, wybierane jest globalne minimum CMNDF.

\subsubsection{Krok 4 - Interpolacja paraboliczna}

W ostatnim kroku na podstawie wartości $\tau_d$ oraz jej najbliższych sąsiadów wyliczana jest interpolacja paraboliczna. Znaleziona odcięta minimum paraboli wyznacza $\tau'_d$. Szukana częstotliwość $f_0$ można wyznaczyć z zależności \ref{ACF-f0}.

W pracy podkreślany jest fakt nieznacznego obciążenia wartości $\tau'_d$. W celu uniknięcia błędów, do wyliczenia wartości $f_0$ może posłużyć minimum funkcji SDF odpowiadające wartości $\tau'_d$.

\subsection{Krótkoczasowa transformata Fouriera (STFT)}

Krótkoczasowa transformata Fouriera (STFT) jest przekształceniem stosowanym do zarejestrowania zmian częstotliwości sygnału w czasie. Polega na obliczaniu dyskretnej transformaty Fouriera (DFT) dla krótkiego okna - stąd nazwa, w którym zakłada się stacjonarność sygnału \cite{OSOWSKI16}. Rezultatem STFT jest funkcja zależna od częstotliwości oraz czasu. Wzór dla przypadku skończonego sygnału dyskretnego ma postać:

\begin{equation}
\label{AMDF}
STFT(\tau, \omega) = \sum_{n=0}^{N-1} x(n)h(n-\tau)e^{-j\omega n}
\end{equation}

\noindent
gdzie: \\
$x(n)$ jest zdyskretyzowaną postacią sygnału \\
$h(n - \tau)$ jest funkcją przesuwającego się okna. \\

Wynik krótkoczasowej transformaty Fouriera jest najczęściej przedstawiany w postaci wykresu nazywanego spektrogramem, który jest definiowany jako kwadrat modułu STFT. Częstotliwość tonu podstawowego $f_0$ może być odczytana bezpośrednio ze spektrogramu.

Transformata Fouriera jest jedną z najczęściej stosowanych technik analizy używanych w cyfrowym przetwarzaniu sygnałów, jednak z jej poprawnym wykorzystaniem wiąże się wiele zagadnień, które wpływają na jakość otrzymywanych wyników.

\subsubsection{Dobór szerokości okna}

Wybór okna w przekształceniu STFT jest swego rodzaju kompromisem pomiędzy rozdzielczością czasową a częstotliwościową. Ograniczeniem jest tzw. zasada nieoznaczoności Gabora, wyrażająca się zależnością \cite{OSOWSKI16}:

\begin{equation}
\label{AMDF}
\Delta t \Delta f \geq \frac{1}{4\pi}
\end{equation}

Konsekwencją tego ograniczenia jest fakt, że szerokość okna wyznacza jednoznacznie rozdzielczość czasową i częstotliwościową. Dla sygnału próbkowanego z częstotliwością $f_s$ i okna o szerokości $M$ próbek, rozdzielczość częstotliwościowa $\Delta f$ wyraża się następująco:

\begin{equation}
\Delta f = \frac{f_s}{M}
\end{equation}

\subsubsection{Dobór kształtu okna}

Wybór kształtu okna związany jest z efektem skończonej długości okna i wynikającym z niego zjawiskiem przecieku energii w widmie częstotliwościowym sygnału \cite{OSOWSKI16}.

W teorii transformata DFT obliczana jest dla nieskończenie długiego sygnału. W praktyce dysponuje się jedynie sygnałami skończonymi, a uzyskiwany ciąg pomiarowy jest iloczynem oryginalnego sygnału i okna. Gdy nie dokonano żadnych dodatkowych operacji, odpowiada to wybraniu okna prostokątnego. Zdefiniowane jest ono jako:

\begin{equation}
\label{macierz}
w_p(n) = \left\{ \begin{array}{ll}
1, & \textrm{gdy $ 0 \leq n \leq M$} \\
0, & \textrm{poza}
\end{array}
\right.
\end{equation}

Jeżeli $X(\omega)$ oznacza transformatę Fouriera oryginalnego sygnału, to transformata analizowanego w rzeczywistości, skończonego sygnału $X_m(\omega)$ wyraża się jako splot transformaty sygnału $X(\omega)$ i okna $W(\omega)$:

\begin{equation}
X_m(\omega) = \frac{1}{2\pi} X(\omega) \ast W(\omega)
\end{equation}

Jeżeli długość okna jest całkowitą wielokrotnością okresu sygnału, to wyniki transformaty DFT nie będą obarczone błędem wynikającym z rozważania splotu, a nie samego sygnału. Jest tak dlatego, że wyniki DFT przypadną wtedy w miejscach, które odpowiadają punktom węzłowym splotu widma ciągłej transformaty Fouriera sygnałów (DTFT) \cite{OSOWSKI16}.
W praktyce jednak najczęściej nie dysponuje się wiedzą na temat okresu próbkowanego sygnału, przez co wynik DFT obarczony jest błędem. W przypadku widma funkcji sinusoidalnej może się okazać, że zamiast pojedynczego prążka widma częstotliwościowego rezultatem jest kilka w najbliższym otoczeniu częstotliwości sinusoidy. Efekt ten nazywany jest przeciekiem energii widma częstotliwościowego.  

W celu ograniczenia błędów stosuje się inne kształty okien funkcyjnych. Efektem jest zmniejszenie się obszaru ,,rozlewania'' się widma. Ceną za zmniejszenie negatywnego efektu jest mniejsza rozdzielczość częstotliwościowa. W przetwarzaniu sygnałów najczęściej korzysta się z okien Hanna $w_{hann}(n)$ i Hamminga  $w_{hamm}(n)$, które są kompromisem między efektem wycieku energii widma, a uzyskaną rozdzielczością częstotliwościową. Wspomniane okna opisane są następującymi zależnościami:

\begin{equation}
\label{macierz}
w_{hann}(n) = \left\{ \begin{array}{ll}
0,5 \cdot (1 - cos(2 \pi n/ M)), & \textrm{gdy $ 0 \leq n \leq M$} \\
0, & \textrm{poza}
\end{array}
\right.
\end{equation}

\begin{equation}
\label{macierz}
w_{hamm}(n) = \left\{ \begin{array}{ll}
0,08 + 0,46 \cdot (1 - cos(2 \pi t / M)), & \textrm{gdy $ 0 \leq n \leq M$} \\
0, & \textrm{poza}
\end{array}
\right.
\end{equation}

\subsubsection{Uzupełnianie zerami}

Algorytm szybkiej transformaty Fouriera (FFT) posiada dużo mniejszą złożoność obliczeniową niż implementacja bazująca na definicji dyskretnej transformaty Fouriera. FFT wymaga jednak, aby długość spróbowanego sygnału była całkowitą potęgą liczby 2. Niemniej często wykorzystywany w analizie sygnał nie spełnia tego warunku. W takich wypadkach można skorzystać z uzupełniania sygnału zerami w celu uzyskania pożądanej jego długości. 

Stosując tego typu zabieg należy jednak pamiętać, że do mierzonego sygnału nie są dodawane żadne dodatkowe informacje. W związku z tym faktyczna rozdzielczość częstotliwościowa sygnału nie ulega zmianie. Fakt obecności dodatkowych próbek powoduje jedynie interpolację widma w punktach wynikających ze zwiększonej długości sygnału.

\section{Wykrywanie początków nut}

\subsection{Ważenie energii sygnału}

Jedną z najprostszych możliwości wydobycia informacji o pojawieniu się nowych dźwięków jest wykrywanie zmian w energii sygnału. Można tego dokonać za pomocą prostej funkcji, która dokonuje średniej ważonej energii widma sygnału \cite{BELLO05-Tutorial}:

\begin{equation}
\tilde{E}(n) = \frac{1}{N} \sum_{k = 0}^{N - 1} W_k |X_k(n)|^2
\end{equation}

Mając na uwadze fakt, że zmiany w natężeniu dźwięków są bardziej zauważalne dla wysokich częstotliwości można dobrać wagi $W_k$, tak aby były większe dla tych zakresów~\cite{BELLO05-Tutorial}.


\subsection{\textit{Beat spectrum}}

\label{subsec:4_beat}

Ciekawą metodę wykrywania początków nut można znaleźć w \cite{FOOTE01-Beat}. Opiera się ona na konstrukcji odpowiednika spektrum dla rytmu, które jest tworzone na podstawie macierzy podobieństwa utworu w czasie. Algorytm został przedstawiony poniżej:

\subsubsection{Krok 1 - Parametryzacja}

Pierwszym krokiem jest parametryzacja sygnału, korzystając np. z transformacji Fouriera lub melowych współczynników cepstralnych. Wybrana forma reprezentacji sygnału może wydobywać różne jego parametry.

We wspomnianej pracy liczona jest transformata Fouriera za pomocą ramki o 256 próbkach, przesuwana w czasie o 128 próbek. Następnie dla każdej ramki liczony jest logarytm modułu transformaty, który zapisywany jest jako wektor opisujący własności ramki w danym momencie czasu wynikającym z przesunięcia ramki.

\subsubsection{Krok 2 - Utworzenie macierzy podobieństwa}
Drugim krokiem algorytmu jest utworzenie macierzy podobieństwa. Do jej konstrukcji potrzeba jest miara określająca podobieństwo między dwoma wektorami parametrów. Jedna z możliwości jest zastosowanie kosinusa kąta pomiędzy wektorami:

\begin{equation}
D_C(i,j) = \frac{v_i \circ v_j}{\Arrowvert v_i \Arrowvert \Arrowvert v_j \Arrowvert}
\end{equation}

Macierz podobieństwa $S$ zawiera wartości funkcji podobieństwa obliczone dla wszystkich możliwych kombinacji wektorów. Tworzy się ją tak, że element $S_{ij} = D(i,j)$.

\subsubsection{Krok 3 - Utworzenie \textit{beat spectrum}}

\textit{Beat spectrum} $B(l)$ to funkcja określająca podobieństwo danego fragmentu macierzy $S$ do siebie oddalonego o przesunięcie $l$. W najprostszy sposób $B(l)$ można obliczyć jako:

\begin{equation}
B(l) \approx \sum_{k \subset R} S(k, k+l)
\end{equation}

Bardziej wiarygodną estymacją beat spectrum jest jednak funkcja autokorelacji wyrażona przez:

\begin{equation}
B(k,l) = \sum_{i,j} S(i,j)S(i+k, j+l)
\end{equation}

\noindent
gdzie: \\
$k$ i $l$ to przesunięcia w pionie i poziomie macierzy podobieństwa \\

Z racji symetryczności $B(k, l)$ wystarczy ograniczyć się do operacji sumowania po jednej zmiennej, otrzymując jednowymiarową funkcję $B(l)$

Z funkcji $B(l)$ liczonych dla pojedynczych ramek można utworzyć obraz podobny do spektrogramu, określający ewolucję podobieństwa sygnału w czasie. Aby to zrobić należy uwzględnić sekwencję wartości $B(l)$, podając wartości funkcji dla kolejnych ramek przesunięte w czasie o $\tau$. Skutkuje to dwuwymiarowym obrazem $B(l, \tau)$.

Przy pomocy \textit{beat spectrum} można w łatwy sposób określić strukturę utworu. Wierzchołki w funkcji $B(l)$ pokazują podstawowe okresowości w rytmice utworu.

\subsubsection{Dodatkowy Krok 4 - Wyznaczenie początków dźwięków}

Podobieństwo czasowe utworu nie określa jednak dokładnych czasów początków nut. Aby je wyznaczyć, należy zastosować korelację S z jądrem, które jest kształtu szachownicy \cite{FOOTE00-Kernel}. Najprostszy przykład takiego jądra to:

\begin{equation}
C = \begin{bmatrix}
1 & -1 \\
-1 & 1 
\end{bmatrix}
\end{equation}

\noindent
Korelację należy wyliczyć wzdłuż diagonali $S$ według wzoru:

\begin{equation}
N(i) = \sum_{m= -L/2}^{L/2} \sum_{n = -L/2}^{L/2} C(m,n)S(i+m, i+n)
\label{eq:novelty}
\end{equation}

\noindent
gdzie: \\
$L$ - długość jądra \\

 Otrzymuje się w ten sposób funkcję pokazującą zmienność sygnału audio w czasie. Dużym wartościom odpowiadają miejsca o dużej zmienności w nagraniu - np. początki dźwięków.



\section{Detekcja aktywności mówcy}

Kolejnym istotnym elementem wpływającym na niezawodność systemu automatycznej transkrypcji muzyki jest tzw. detekcja aktywności mówcy. Również w tym wypadku można zaczerpnąć z teorii ukształtowanej wokół metod przetwarzania sygnału mowy. Proste algorytmy dotyczące tego zagadnienia można znaleźć np. w \cite{MAKOWSKI11-ARM}. Bazują one na obliczaniach energii sygnału bądź też jego amplitudy i zastosowaniu globalnego progu określającego aktywność mówcy.

\subsection{Detekcja na podstawie amplitudy sygnału}
\label{subsec:4_ampl}

Zakładając ramkę danych długości $M$, można dokonać szybkiej analizy aktywności mówcy na podstawie wzoru:

\begin{equation}
A = \sum_{n = 0}^{M - 1} |x(n)|
\end{equation}

Korzystniejsze jednak z punktu widzenia uniwersalności jest zastosowanie pewnej normalizacji, odnosząc wynik do długości ramki. Daje to możliwość stosowania tego samego progu dla różnych długości ramek. Zmodyfikowany wzór został przedstawiony poniżej:

\begin{equation}
A' = \frac{1}{M} \sum_{n = 0}^{M - 1} |x(n)|
\end{equation}

\subsection{Detekcja na podstawie energii sygnału}

Analogicznie w celu detekcji aktywności mówcy można posłużyć się energią sygnału, wyliczoną na podstawie ramki. Wzór odnoszący wartość energii do długości ramki, ma postać:

\begin{equation}
E = \frac{1}{M} \sum_{n = 0}^{M -1} x(n)^2
\end{equation}


\section{Wykrywanie tempa utworu}

Następnym zagadnieniem związanym z wydobywaniem informacji z utworów muzycznych jest określanie ich tempa. Zadanie to jest powiązane z określaniem początków nut dlatego, że przeważająca część zmian dźwięków i akordów w utworach muzycznych przypada na główne wartości podziału rytmicznego. Stąd też spora część algorytmów wykrywania tempa bazuje na wyznaczeniu początków dźwięków w pierwszym kroku. Inne podejście prezentuje np. metoda odczytywania tempa korzystając ze wspomnianego beat spectrum.

\subsection{Sumowanie funkcji autokowariancji}

Ciekawą i stosunkowo prostą metodą wyznaczania tempa utworu jest wykorzystanie sumowania funkcji autokowariancji dla różnych zakresów częstotliwości sygnału. Algorytm, przedstawiony poniżej, został zaprezentowany w \cite{Alonso03-Tempo}.

\subsubsection{Krok 1 - Podział na zakresy}

W pierwszym kroku analizowane nagranie muzyczne jest dzielone według częstotliwości na osiem niepokrywających się zakresów częstotliwości korzystając z filtrów Butterworth'a szóstego rzędu.
Pierwszy zakres częstotliwości obejmuje wszystko do $100Hz$, pozostałe siedem zakresów dobrane jest logarytmicznie od $100Hz$ do połowy częstotliwości próbkowania. W przypadku analizowanego systemu do $8000Hz$.

\subsubsection{Krok 2 - Wyznaczenie początków nut}

Na początku należy wyznaczyć obwiednię sygnału. W związku z tym z każdego zakresu częstotliwości wycinana jest jedna połowa amplitudy. Następnie całość podnoszona jest do kwadratu i filtrowana dolnoprzepustowo celem wydobycia obwiedni. W oryginalnym systemie zastosowano splot z opadającą połową okna Hanna długości $100ms$. 

Kolejnym krokiem służącym zmniejszeniu ilości dalszych obliczeń jest decymacja sygnału. Dlatego pozostawia się co 16 próbkę z przetworzonej dotychczas części nagrania. 

Następnie stosowane jest przekształcenie odnoszące zmiany w analizowanym sygnale do jego bezwzględnego poziomu. Operację można przeprowadzić, różniczkując uzyskaną obwiednię zgodnie z wzorem:

\begin{equation}
W_k(t) = \frac{d}{dt}(log(A_k(t))
\end{equation}

\noindent
gdzie: \\
$A_k(t)$ - Analizowany sygnał odpowiadający danemu zakresowi częstotliwości \\

Ostatnim krokiem jest wyznaczenie progu. Wszystkie wartości powyżej niego są klasyfikowane jako początki nut. W pracy został on eksperymentalny dobrany na poziomie $1.5\sigma_{w_k}$. Gdzie $\sigma_{w_k}$ oznacza odchylenie standardowe sygnału $W_k(t)$.

\subsubsection{Krok 3 - Wyznaczenie tempa}

Aby wyznaczyć tempo utworu, na początku wyjściowy ciąg impulsów z detektora początków dźwięków jest splatany z oknem Hanna nieparzystej długości. Następnie dla każdego tak przekształconego zakresu częstotliwości liczona jest funkcja autokowariancji:

\begin{equation}
\Gamma_k(\tau) = \sum_{t} [x_k(t+\tau) - \overline{x}_k][x_k(t) - \overline{x}_k]
\end{equation}

\noindent
gdzie: \\
$\overline{x}_k$ - średnia z sygnału $x_k$ \\

Następnie obliczone funkcje autokowariancji dla każdego zakresu częstotliwości są dodawane do siebie:

\begin{equation}
SACVF(\tau) = \sum_{k=0}^{7} \Gamma_k(\tau)
\end{equation}

Ostatnim krokiem jest odczytanie maksimum funkcji $SACVF(\tau)$, w zakresie odpowiadającym spodziewanym wynikom. Jeżeli przyjąć, że w ciągu minuty może pojawić się od 50 do 300 uderzeń na minutę (BPM), to przeszukiwany zakres będzie obejmował od $250$ do $1250ms$. Znaleziony argument maksymalizujący funkcję w podanym zakresie, można z powrotem przeliczyć na BPM.

\section{Wykrywanie tonacji utworu}

\label{subsec:key}

Do wykrywania tonacji utworu zastosowano algorytm Krumhansl'a-Schmuckler'a \cite{TEMPERLEY99-key}. Służy on do wyznaczenia tonacji na podstawie nut tworzących zapis nutowy.

W pierwszym kroku wyznaczane są sumaryczne czasy trwania poszczególnych dźwięków utworu. Z uzyskanych czasów tworzony jest dwunasto-elementowy wektor danych wejściowych. Następnie liczone są współczynniki korelacji wektora wyjściowego z wektorami profili tonacji durowych i molowych rozpoczynającymi się od każdego z dwunastu dźwięków oktawy. Obliczenia prowadzone są według wzoru:
\begin{equation}
r = \frac{\sum (x - \overline{x})(y - \overline{y})}{\sqrt{\sum (x - \overline{x})^2}(y - \overline{y})^2}
\end{equation}

\noindent
gdzie: \\
$x$ - wartości wektora wejściowego \\
$y$ - wartości wektora profilu \\ 


Wektory profili zostały wyznaczone eksperymentalnie i opisane są w \cite{KRUMHANSL01-key}. Eksperyment, który posłużył wyznaczeniu wartości elementów składowych wektorów profili polegał na tym, że grupa badanych miała określić jak poszczególne dźwięki pasują do tonacji wyznaczonej przez kadencję lub odegraną skalę. Wartości poszczególnych wektorów są takie same dla różnych dźwięków początkowych.

Wektory profili są tworzone przez wartości podane w tabelach:

\begin{table}[H]
	\centering
	\begin{tabular}{| c | c | c | c | c | c | c | c | c | c | c | c |}
		\hline
		$C$ & $C\#$ & $D$ & $E \flat $ & $E$ & $F$ & $F\#$ & $G$ & $A \flat$ & $A$ & $B$ & $H$ \\ \hline \hline
		6.35 & 2.23 & 3.48 & 2.33 & 4.38 & 4.09 & 2.52 & 5.19 & 2.39 & 3.66 & 2.29 & 2.88 \\ \hline
	\end{tabular}
	\caption{Wartości wektora profilu tonacji durowej na przykładzie gamy C-dur}
\end{table}

\begin{table}[H]
	\centering
	\begin{tabular}{| c | c | c | c | c | c | c | c | c | c | c | c |}
		\hline
		$C$ & $C\#$ & $D$ & $E \flat $ & $E$ & $F$ & $F\#$ & $G$ & $A \flat$ & $A$ & $B$ & $H$ \\ \hline \hline
		6.33 & 2.68 & 3.52 & 5.38 & 2.60 & 3.53 & 2.54 & 4.75 & 3.98 & 2.69 & 3.34 & 3.17 \\ \hline
	\end{tabular}
	\caption{Wartości wektora profilu tonacji molowej na przykładzie gamy c-moll}
\end{table}



